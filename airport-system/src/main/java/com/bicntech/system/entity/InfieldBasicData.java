package com.bicntech.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.bicntech.common.core.domain.BaseEntity;
import com.bicntech.common.typehandler.JsonArrayTypeHandler;
import com.bicntech.system.dto.InfieldBasicSubjectDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 内场检查项基础数据
 * </p>
 *
 * @author bicntech
 * @since 2022-08-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(autoResultMap = true)
public class InfieldBasicData extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 检查单类型
     */
    private Integer type;

    /**
     * 检查单名称
     */
    private String typeName;

    /**
     * 是否启用  0-禁用,1-启用
     */
    private Boolean enable;

    /**
     * 工卡号
     */
    private String jobCardNo;

    /**
     * 适用车型
     */
    private String applicableModels;

    /**
     * 车牌号
     */
    @TableField(typeHandler = JsonArrayTypeHandler.class)
    private List<String> applicableModelsData;

    /**
     * 版本号
     */
    private String code;

    /**
     * 修订时间
     */
    private Date reviseTime;

    /**
     * 参考标准
     */
    private String referenceStandard;

    /**
     * 检查项内容
     */
    @TableField(typeHandler = JsonArrayTypeHandler.class)
    private List<InfieldBasicSubjectDTO> content;

    /**
     * 是否为特种车辆检查单
     */
    private Boolean isSpecialVehicle;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private String delFlag;

}
