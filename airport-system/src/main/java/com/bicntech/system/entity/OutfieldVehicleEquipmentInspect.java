package com.bicntech.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.bicntech.common.core.domain.BaseEntity;
import com.bicntech.common.typehandler.JsonArrayTypeHandler;
import com.bicntech.system.dto.OutfieldInspectDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;


/**
 * <p>
 * 机务维修记录--外场
 * </p>
 *
 * @author bicntech
 * @since 2022-08-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(autoResultMap = true)
public class OutfieldVehicleEquipmentInspect extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 检查单编号
     */
    private String code;

    /**
     * 检查单类型 1接机工作 2送机工作  3维修检查  4长短停及航后离机检查项目
     */
    private Integer type;

    /**
     * 检查单状态 1待检查  2待复核  3 历史（已完成） 4 废弃
     */
    private Integer state;

    /**
     * 工单日期
     */
    private Date inspectDate;

    /**
     * 航班号
     */
    private String flightNumber;

    /**
     * 机号
     */
    private String machineNumber;

    /**
     * 停机位
     */
    private String stand;

    /**
     * 观察员--检查项
     */
    @TableField(typeHandler = JsonArrayTypeHandler.class)
    private List<OutfieldInspectDTO> workerInspectTerm;

    /**
     * 审核员--检查项
     */
    @TableField(typeHandler = JsonArrayTypeHandler.class)
    private List<OutfieldInspectDTO> reviewerInspectTerm;

    /**
     * 复核者
     */
    private String reviewer;

    /**
     * 复核者id
     */
    private String reviewerId;

    /**
     * 工作者
     */
    private String worker;

    /**
     * 工作者id
     */
    private String workerId;

    /**
     * 复核时间
     */
    private Date reviewerTime;

    /**
     * 检查时间
     */
    private Date inspectTime;

    /**
     * 版本号
     */
    @Version
    private Integer version;

    // /**
    //  * 删除标识。0-正常,1-已删除
    //  */
    // @TableField(fill = FieldFill.INSERT)
    // @TableLogic
    // private String delFlag;

}
