package com.bicntech.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.bicntech.common.core.domain.BaseEntity;
import com.bicntech.common.typehandler.JsonArrayTypeHandler;
import com.bicntech.system.dto.InfieldInspectDataDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 机务维修记录--内场
 * </p>
 *
 * @author bicntech
 * @since 2022-08-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(autoResultMap = true)
public class InfieldVehicleEquipmentInspect extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 检查单编号
     */
    private String code;

    /**
     * 检查单id
     */
    private Long type;

    /**
     * 检查日期
     */
    private Date inspectDate;

    /**
     * 车号
     */
    private String wagonNumber;

    /**
     * 适用车型
     */
    private String applicableModels;

    /**
     * 编写人
     */
    private String preparedBy;

    /**
     * 审核
     */
    private String toExamine;

    /**
     * 批准
     */
    private String approval;

    /**
     * 检查项
     */
    @TableField(typeHandler = JsonArrayTypeHandler.class)
    private List<InfieldInspectDataDTO> inspectTerm;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 车辆摩托小时
     */
    private BigDecimal vehicleMotorcycleHours;

    /**
     * 补加油量
     */
    private BigDecimal refuelingVolume;

    /**
     * 检查人签名
     */
    private String signatureOfInspector;

    /**
     * 检查人id
     */
    private String signatureOfInspectorId;

    /**
     * 异常项
     */
    private String isAbnormal;

    /**
     * 版本号
     */
    @Version
    private Integer version;

    // /**
    //  * 删除标识。0-正常,1-已删除
    //  */
    // @TableField(fill = FieldFill.INSERT)
    // @TableLogic
    // private String delFlag;


}
