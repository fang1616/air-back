package com.bicntech.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.bicntech.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 * @author 22958
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(autoResultMap = true)
public class UserInfo extends BaseEntity {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户状态
     */
    private Integer userStatus;

    /**
     * 用户组织id
     */
    private Long departmentId;

    /**
     * 用户组织名称
     */
    private String departmentName;

    /**
     * 用户信息json
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private Map<String, Object> userInfoJson;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private String delFlag;

}
