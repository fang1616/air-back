package com.bicntech.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.bicntech.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 人员绩效管理
 * </p>
 *
 * @author bicntech
 * @since 2023-08-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(autoResultMap = true)
public class PersonnelPerformanceManagement extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 年月
     */
    private String years;

    /**
     * 分数
     */
    private BigDecimal mark;

    /**
     * 加分场景 1 内场 2 外场
     */
    private Integer sceneType;

    /**
     * 巡检项计数 k 巡检项 v次数
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private Map<String, Integer> inspectionItem;

    /**
     * 版本号
     */
    @Version
    private Integer version;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private String delFlag;
}
