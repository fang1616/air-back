package com.bicntech.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.bicntech.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 * 绩效加分项配置
 * </p>
 *
 * @author bicntech
 * @since 2023-08-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PerformanceScoreConfig extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 加分项名称
     */
    private String name;

    /**
     * 加分场景 1 内场 2 外场
     */
    private Integer sceneType;

    /**
     * 巡检项
     */
    private Long inspectionItem;

    /**
     * 分数
     */
    private BigDecimal mark;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private String delFlag;

}
