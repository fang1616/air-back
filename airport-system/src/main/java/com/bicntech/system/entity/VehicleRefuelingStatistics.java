package com.bicntech.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.bicntech.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 * 车辆加油统计
 * </p>
 *
 * @author bicntech
 * @since 2023-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class VehicleRefuelingStatistics extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 年
     */
    private Integer year;

    /**
     * 月
     */
    private Integer month;

    /**
     * 车号
     */
    private String wagonNumber;

    /**
     * 补加油量
     */
    private BigDecimal refuelingVolume;


    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private String delFlag;

    /**
     * 版本号
     */
    @Version
    private Integer version;


}
