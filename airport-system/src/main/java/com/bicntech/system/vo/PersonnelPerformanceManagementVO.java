package com.bicntech.system.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

/**
 * 人员绩效管理 VO
 *
 * @author : JMY
 * @create 2023/8/7 13:07
 */
@Data
@ApiModel("人员绩效管理 VO")
public class PersonnelPerformanceManagementVO implements Serializable {
    private static final long serialVersionUID = -4319780841411832191L;

    @ApiModelProperty(value = "id")
    @ExcelIgnore
    private Long id;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    @ExcelIgnore
    private String userId;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    @ExcelProperty(value = "姓名")
    private String userName;

    /**
     * 组织名称
     */
    @ApiModelProperty(value = "组织名称")
    @ExcelProperty(value = "组织名称")
    private String departmentName;

    /**
     * 加分场景 1 内场 2 外场
     */
    @ApiModelProperty(value = "加分场景 1 内场 2 外场")
    @ExcelProperty(value = "检查类型")
    private String sceneType;

    /**
     * 各巡检项巡检次数统计
     */
    @ApiModelProperty(value = "各巡检项巡检次数统计")
    @ExcelIgnore
    private Map<String, Integer> inspectionItem;

    /**
     * 年月
     */
    @ApiModelProperty(value = "年月")
    @ExcelProperty(value = "年-月")
    private String years;

    /**
     * 分数
     */
    @ApiModelProperty(value = "分数")
    @ExcelProperty(value = "分数")
    private BigDecimal mark;

}
