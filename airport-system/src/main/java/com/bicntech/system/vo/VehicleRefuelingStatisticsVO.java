package com.bicntech.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 车辆年加油量统计
 *
 * @author : JMY
 * @create 2023/8/10 10:09
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("车辆年加油量统计")
public class VehicleRefuelingStatisticsVO implements Serializable {
    private static final long serialVersionUID = 1943214379754516350L;

    /**
     * 检查单类型
     */
    @ApiModelProperty(value = "检查单类型")
    private Integer checklistType;

    /**
     * 车辆类型
     */
    @ApiModelProperty(value = "车辆类型")
    private String applicableModels;

    /**
     * 车号
     */
    @ApiModelProperty(value = "车号")
    private String wagonNumber;

    /**
     * 加油量统计数据
     */
    @ApiModelProperty(value = "加油量统计数据")
    private List<FuelCharge> fuelChargeList;


    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @ApiModel("加油量数据")
    public static class FuelCharge implements Serializable {
        private static final long serialVersionUID = -4638916385568630556L;
        /**
         * 年
         */
        @ApiModelProperty(value = "年")
        private Integer year;

        /**
         * 月
         */
        @ApiModelProperty(value = "月")
        private Integer month;

        /**
         * 补加油量
         */
        @ApiModelProperty(value = "补加油量")
        private BigDecimal refuelingVolume;
    }
}
