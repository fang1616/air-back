package com.bicntech.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 外场工单状态统计VO
 *
 * @author 22958
 */
@Data
@Accessors(chain = true)
@ApiModel("外场工单状态统计VO")
public class OutfieldStateStatisticsVO implements Serializable {
    private static final long serialVersionUID = -6990454878698482849L;

    /**
     * 检查单状态 1待检查  2待复核  3 历史 4 废弃
     */
    @ApiModelProperty(value = "检查单状态 1待检查  2待复核  3 历史 4 废弃")
    private Integer state;

    /**
     * 总数
     */
    @ApiModelProperty(value = "总数")
    private Long count;
}
