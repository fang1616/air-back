package com.bicntech.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 企业微信用户VO
 *
 * @author : JMY
 * @create 2023/8/17 9:46
 */
@Data
@ApiModel("企业微信用户VO")
public class UserInfoVO implements Serializable {
    private static final long serialVersionUID = -3722493893825816418L;


    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String userName;

    /**
     * 用户组织id
     */
    @ApiModelProperty(value = "用户组织id")
    private Long departmentId;

    /**
     * 用户组织名称
     */
    @ApiModelProperty(value = "用户组织名称")
    private String departmentName;

}
