package com.bicntech.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 外场工单类型统计VO
 *
 * @author 22958
 */
@Data
@Accessors(chain = true)
@ApiModel("外场工单类型统计VO")
public class OutfieldTypeStatisticsVO implements Serializable {
    private static final long serialVersionUID = -5398510870088155522L;

    /**
     * 检查单类型 1接机工作 2送机工作  3维修检查  4长短停及航后离机检查项目
     */
    @ApiModelProperty(value = "检查单类型 1接机工作 2送机工作  3维修检查  4长短停及航后离机检查项目")
    private Integer type;

    /**
     * 总数
     */
    @ApiModelProperty(value = "总数")
    private Long count;
}