package com.bicntech.system.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 微信用户信息
 *
 * @author : JMY
 * @create 2023/8/9 9:46
 */
@Data
public class WeiXinUserInfoDTO implements Serializable {
    private static final long serialVersionUID = 8365091355681565588L;

    @JsonProperty(value = "errcode")
    private Integer errCode;

    @JsonProperty(value = "errmsg")
    private String errMsg;

    @JsonProperty(value = "userid")
    private String userId;

    private String name;

    private List<Integer> department;

    private String position;

    private Integer status;

    @JsonProperty(value = "isleader")
    private Integer isLeader;

    private Map<String, Object> extattr;

    private String telephone;

    private Integer enable;

    @JsonProperty(value = "hide_mobile")
    private Integer hideMobile;

    private List<Integer> order;

    @JsonProperty(value = "main_department")
    private Integer mainDepartment;

    private String alias;

    @JsonProperty(value = "is_leader_in_dept")
    private List<Integer> isLeaderInDept;

    @JsonProperty(value = "direct_leader")
    private List<String> directLeader;

}
