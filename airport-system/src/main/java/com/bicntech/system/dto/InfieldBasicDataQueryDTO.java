package com.bicntech.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

/**
 * @author jmy
 */
@Data
@ApiModel("查询内场检验数据")
public class InfieldBasicDataQueryDTO implements Serializable {
    /**
     * 检查单id
     */
    @ApiModelProperty(required = false, notes = "检查单id")
    private Long type;


    /**
     * 检查单名称
     */
    @ApiModelProperty(required = false, notes = "检查单名称")
    private String typeName;

    /**
     * 适用车型
     */
    @ApiModelProperty(required = false, notes = "适用车型")
    private String applicableModels;


    /**
     * 车号
     */
    @ApiModelProperty(required = false, notes = "车号")
    private String wagonNumber;

    /**
     * 选择的日期
     */
    @ApiModelProperty(required = false, notes = "选择的日期")
    private LocalDate dateView;

    /**
     * 是否查询我的检查单
     */
    @ApiModelProperty(required = false, notes = "是否查询我的检查单（微信端专用）")
    private Boolean isMe;

    /**
     * 开始时间
     */
    @ApiModelProperty(required = false, notes = "开始时间")
    private Date startTime;

    /**
     * 结束时间
     */
    @ApiModelProperty(required = false, notes = "结束时间")
    private Date endTime;

}
