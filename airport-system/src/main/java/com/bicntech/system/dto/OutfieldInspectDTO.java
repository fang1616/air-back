package com.bicntech.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author jmy
 */
@Data
@ApiModel("外场检查项")
public class OutfieldInspectDTO implements Serializable {
    private static final long serialVersionUID = 3381354469331347077L;

    /**
     * 检查项
     */
    @ApiModelProperty("检查项")
    @NotNull
    private Integer index;

    /**
     * 检查结果
     */
    @ApiModelProperty("检查结果")
    private Object content;
}
