package com.bicntech.system.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 内场excel导出模板
 *
 * @author 22958
 */
@Data
@Accessors(chain = true)
public class InfieldVehicleExcelDTO implements Serializable {
    private static final long serialVersionUID = 2044533927794843759L;

    /**
     * 检查单类型
     */
    @ExcelProperty(value = "检查单类型")
    private String typeName;

    /**
     * 适用车型
     */
    @ExcelProperty(value = "适用车型")
    private String applicableModels;

    /**
     * 车号
     */
    @ExcelProperty(value = "车号")
    private String wagonNumber;

    /**
     * 检查日期
     */
    @ExcelProperty(value = "检查日期")
    private String inspectDate;

    /**
     * 备注
     */
    @ExcelIgnore
    @ExcelProperty(value = "备注")
    private String remarks;

    /**
     * 车辆摩托小时
     */
    @ExcelIgnore
    @ExcelProperty(value = "车辆摩托小时")
    private BigDecimal vehicleMotorcycleHours;

    /**
     * 补加油量
     */
    @ExcelIgnore
    @ExcelProperty(value = "补加油量")
    private BigDecimal refuelingVolume;

    /**
     * 检查人签名
     */
    @ExcelIgnore
    @ExcelProperty(value = "检查人")
    private String signatureOfInspector;
}
