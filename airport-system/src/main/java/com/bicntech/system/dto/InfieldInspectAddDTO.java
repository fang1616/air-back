package com.bicntech.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author jmy
 */
@Data
@ApiModel("内场巡检新增入参")
public class InfieldInspectAddDTO implements Serializable {
    private static final long serialVersionUID = -319446840526982831L;

    /**
     * 检查单id
     */
    @ApiModelProperty(notes = "检查单id")
    @NotNull
    private Long type;

    /**
     * 检查日期
     */
    @ApiModelProperty(notes = "检查日期")
    @NotNull
    // @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date inspectDate;


    /**
     * 适用车型
     */
    @ApiModelProperty(required = false, notes = "适用车型")
    @NotBlank
    private String applicableModels;

    /**
     * 车号
     */
    @ApiModelProperty(notes = "车号")
    @NotBlank
    private String wagonNumber;

    /**
     * 检查项
     */
    @ApiModelProperty(notes = "检查项")
    @NotNull
    private List<InfieldInspectDataDTO> inspectTerm;


    /**
     * 备注
     */
    @ApiModelProperty(required = false, notes = "备注")
    private String remarks;


    /**
     * 车辆摩托小时
     */
    @ApiModelProperty(required = false, notes = "车辆摩托小时")
    @NotNull(message = "vehicleMotorcycleHours is not null")
    @Digits(integer = 9, fraction = 3, message = "车辆摩托小时参数异常，请填写合理数值")
    private BigDecimal vehicleMotorcycleHours;


    /**
     * 补加油量
     */
    @ApiModelProperty(required = false, notes = "补加油量")
    @NotNull(message = "refuelingVolume is not null")
    @Digits(integer = 9, fraction = 3, message = "补加油量参数异常，请填写合理数值")
    private BigDecimal refuelingVolume;


}
