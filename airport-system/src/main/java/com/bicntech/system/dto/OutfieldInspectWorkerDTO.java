package com.bicntech.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author jmy
 */
@Data
@ApiModel("外场检查结果")
public class OutfieldInspectWorkerDTO implements Serializable {

    /**
     * 工单id
     */
    @ApiModelProperty(notes = "工单id")
    @NotNull
    private Long id;

    /**
     * 观察员
     */
    @ApiModelProperty(notes = "观察员")
    @NotNull
    private String name;

    /**
     * 检查项
     */
    @ApiModelProperty(notes = "检查项")
    @NotNull
    private List<OutfieldInspectDTO> inspectDTOS;

    /**
     * 是否完成检查单（直接将状态修改为历史，跳过待审核）
     */
    @ApiModelProperty(notes = "是否完成检查单")
    private Boolean isFinish;
}
