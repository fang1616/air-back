package com.bicntech.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author jmy
 */
@Data
@ApiModel("内场巡检项")
public class InfieldBasicSubjectDTO implements Serializable {
    private static final long serialVersionUID = 3291693382483945916L;

    /**
     * 巡检项
     */
    @ApiModelProperty(required = false, notes = "巡检项")
    private Integer index;

    /**
     * 检查项内容
     */
    @ApiModelProperty(required = false, notes = "检查项内容")
    private String subject;
    /**
     * 1 勾选或填写  2填写
     */
    @ApiModelProperty(required = false, notes = " 1 勾选或填写  2填写")
    private Integer contentType;

    /**
     * 填写内容数量
     */
    @ApiModelProperty(required = false, notes = "填写内容数量")
    private Integer contentNumber;

    /**
     * 状态 默认空为开启 1开启 2关闭
     */
    @ApiModelProperty(required = false, notes = "状态 默认空为开启 1开启 2关闭")
    private Integer status;

}
