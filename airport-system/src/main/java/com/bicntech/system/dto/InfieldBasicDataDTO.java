package com.bicntech.system.dto;


import com.bicntech.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * @author bicntech
 * @description: 内场检查项基础数据 传输实体类
 * </p>
 * @date 2022-08-02
 */
@ApiModel("内场检查项基础数据")
@Data
public class InfieldBasicDataDTO extends BaseEntity {

    private static final long serialVersionUID = -2014336270485293657L;

    @ApiModelProperty(required = false, notes = "id")
    private Long id;


    /**
     * 检查单类型
     */
    @ApiModelProperty(required = false, notes = "检查单类型")
    private Integer type;


    /**
     * 检查单名称
     */
    @ApiModelProperty(required = false, notes = "检查单名称")
    private String typeName;


    /**
     * 是否启用  0-禁用,1-启用
     */
    @ApiModelProperty(required = false, notes = "是否启用  0-禁用,1-启用")
    private Boolean enable;


    /**
     * 工卡号
     */
    @ApiModelProperty(required = false, notes = "工卡号")
    private String jobCardNo;


    /**
     * 适用车型
     */
    @ApiModelProperty(required = false, notes = "适用车型")
    private String applicableModels;

    /**
     * 车牌号
     */
    @ApiModelProperty(required = false, notes = "车牌号")
    private List<String> applicableModelsData;

    /**
     * 版本号
     */
    @ApiModelProperty(required = false, notes = "版本号")
    private String code;


    /**
     * 修订时间
     */
    @ApiModelProperty(required = false, notes = "修订时间")
    // @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date reviseTime;


    /**
     * 参考标准
     */
    @ApiModelProperty(required = false, notes = "参考标准")
    private String referenceStandard;


    /**
     * 检查项内容
     */
    @ApiModelProperty(required = false, notes = "检查项内容")
    private List<InfieldBasicSubjectDTO> content;


}
