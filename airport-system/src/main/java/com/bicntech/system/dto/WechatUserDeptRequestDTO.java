package com.bicntech.system.dto;

import com.bicntech.common.core.validate.AddGroup;
import com.bicntech.common.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 微信用户组织管理 add
 *
 * @author : JMY
 * @create 2023/8/16 17:18
 */
@Data
@ApiModel("微信用户组织管理 add")
public class WechatUserDeptRequestDTO implements Serializable {
    private static final long serialVersionUID = -8943255087856696667L;
    /**
     * 部门id
     */
    @ApiModelProperty(value = "父菜单ID 新增不传")
    @NotNull(message = "父菜单ID不能为空", groups = {EditGroup.class})
    private Long deptId;

    /**
     * 父菜单ID
     */
    @ApiModelProperty(value = "父菜单ID", required = true)
//    @NotNull(message = "父菜单ID不能为空", groups = {EditGroup.class, AddGroup.class})
    private Long parentId;

    /**
     * 部门名称
     */
    @ApiModelProperty(value = "部门名称", required = true)
    @NotBlank(message = "部门名称不能为空", groups = {EditGroup.class, AddGroup.class})
    @Size(min = 0, max = 30, message = "部门名称长度不能超过30个字符", groups = {EditGroup.class, AddGroup.class})
    private String deptName;

    /**
     * 显示顺序
     */
    @ApiModelProperty(value = "显示顺序", required = true)
    @NotNull(message = "显示顺序不能为空", groups = {EditGroup.class, AddGroup.class})
    private Integer orderNum;
}
