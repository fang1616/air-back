package com.bicntech.system.dto;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 微信DTO
 *
 * @author : JMY
 * @create 2023/8/9 10:23
 */
@Data
public class WeiXinDepartmentDTO implements Serializable {
    private static final long serialVersionUID = -2712468130068781959L;

    @JsonProperty(value = "errcode")
    private Integer errCode;

    @JsonProperty(value = "errmsg")
    private String errMsg;

    private DepartmentInfo department;

    @Data
    public static class DepartmentInfo implements Serializable {
        private static final long serialVersionUID = -7217342046867854688L;

        private Integer id;

        private String name;

        @JsonProperty(value = "name_en")
        private String nameEn;

        @JsonProperty(value = "department_leader")
        private List<String> departmentLeader;

        @JsonProperty(value = "parentid")
        private Integer parentId;

        private Integer order;

        public String getName() {
            return StrUtil.isNotBlank(this.name) ? this.name.trim() : this.name;
        }
    }
}
