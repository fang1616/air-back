package com.bicntech.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 编辑用户组织
 *
 * @author : JMY
 * @create 2023/8/17 9:31
 */
@Data
@ApiModel("编辑用户组织")
public class UserDeptUpdateDTO implements Serializable {
    private static final long serialVersionUID = 6864278288597122447L;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", required = true)
    @NotNull(message = "userId is not null")
    private Long userId;


    /**
     * 组织id
     */
    @ApiModelProperty(value = "组织id", required = true)
    @NotNull(message = "deptId is not null")
    private Long deptId;

}

