package com.bicntech.system.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 外场excel导出模板
 *
 * @author 22958
 */
@Data
@Accessors(chain = true)
public class OutfieldVehicleExcelDTO implements Serializable {
    private static final long serialVersionUID = 4640107319907510079L;

    /**
     * 工单类型
     */
    @ExcelProperty(value = "工单类型")
    private String typeName;

    /**
     * 工单日期
     */
    @ExcelProperty(value = "日期")
    private String inspectDate;

    /**
     * 航班号
     */
    @ExcelProperty(value = "航班号")
    private String flightNumber;

    /**
     * 机号
     */
    @ExcelProperty(value = "机号")
    private String machineNumber;

    /**
     * 停机位
     */
    @ExcelProperty(value = "停机位")
    private String stand;

    /**
     * 检查单状态 1待检查  2待复核  3 已复核 4 废弃
     */
    @ExcelProperty(value = "工单状态")
    private String state;

    /**
     * 工作者
     */
    @ExcelProperty(value = "工作者")
    private String worker;

    /**
     * 复核者
     */
    @ExcelProperty(value = "复核者")
    private String reviewer;
}
