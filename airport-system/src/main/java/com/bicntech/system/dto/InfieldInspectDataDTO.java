package com.bicntech.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author jmy
 */
@Data
@ApiModel("内场检查项内容")
public class InfieldInspectDataDTO implements Serializable {
    private static final long serialVersionUID = 3559235004632929650L;

    /**
     * 检查项
     */
    @ApiModelProperty("检查项")
    @NotNull
    private Integer index;

    /**
     * 0正常 1异常 2 未安装
     */
    @ApiModelProperty(required = false, notes = "0正常 1异常 2未安装")
    @NotNull
    private Integer isAbnormal;


    /**
     * 检查结果
     */
    @ApiModelProperty("检查结果")
    private List<String> content;
}
