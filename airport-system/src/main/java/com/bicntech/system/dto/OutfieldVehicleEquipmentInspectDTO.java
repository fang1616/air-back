package com.bicntech.system.dto;


import com.bicntech.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * @author bicntech
 * @description:机务维修记录--外场 传输实体类
 * </p>
 * @date 2022-08-02
 */
@ApiModel("机务维修记录--外场")
@Data
public class OutfieldVehicleEquipmentInspectDTO extends BaseEntity {


    private static final long serialVersionUID = 6022515946592842305L;


    @ApiModelProperty(required = false, notes = "id")
    private Long id;


    /**
     * 检查单编号
     */
    @ApiModelProperty(required = false, notes = "检查单编号")
    private String code;


    /**
     * 检查单类型 1接机工作 2送机工作  3维修检查  4长短停及航后离机检查项目
     */
    @ApiModelProperty(required = false, notes = "检查单类型 1接机工作 2送机工作  3维修检查  4长短停及航后离机检查项目")
    private Integer type;


    /**
     * 检查单状态 1待检查  2待复核  3 已复核 4 废弃
     */
    @ApiModelProperty(required = false, notes = "检查单状态 1待检查  2待复核  3 历史 4 废弃")
    private Integer state;


    /**
     * 检查日期
     */
    @ApiModelProperty(required = false, notes = "检查日期")
    // @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date inspectDate;


    /**
     * 航班号
     */
    @ApiModelProperty(required = false, notes = "航班号")
    private String flightNumber;


    /**
     * 机号
     */
    @ApiModelProperty(required = false, notes = "机号")
    private String machineNumber;


    /**
     * 停机位
     */
    @ApiModelProperty(required = false, notes = "停机位")
    private String stand;


    /**
     * 观察员--检查项
     */
    @ApiModelProperty(required = false, notes = "工作者--检查项")
    private List<OutfieldInspectDTO> workerInspectTerm;


    /**
     * 审核员--检查项
     */
    @ApiModelProperty(required = false, notes = "审核员--检查项")
    private List<OutfieldInspectDTO> reviewerInspectTerm;


    /**
     * 复核者
     */
    @ApiModelProperty(required = false, notes = "复核者")
    private String reviewer;

    /**
     * 工作者
     */
    @ApiModelProperty(required = false, notes = "工作者")
    private String worker;


    /**
     * 复核时间
     */
    @ApiModelProperty(required = false, notes = "复核时间")
    private Date reviewerTime;

    /**
     * 复核时间戳
     */
    @ApiModelProperty(required = false, notes = "复核时间戳")
    private Long reviewerTimestamp;

    public Long getReviewerTimestamp() {
        return reviewerTime != null ? reviewerTime.getTime() : null;
    }

    /**
     * 检查时间
     */
    @ApiModelProperty(required = false, notes = "检查时间")
    private Date inspectTime;

    /**
     * 检查时间戳
     */
    @ApiModelProperty(required = false, notes = "检查时间戳")
    private Long inspectTimestamp;

    /**
     * 版本号
     */
    @ApiModelProperty(notes = "版本号")
    private Integer version;

    public Long getInspectTimestamp() {
        return inspectTime != null ? inspectTime.getTime() : null;
    }
}
