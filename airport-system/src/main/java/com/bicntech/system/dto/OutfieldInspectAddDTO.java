package com.bicntech.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author jmy
 */
@Data
@ApiModel("外场巡检表头新增")
public class OutfieldInspectAddDTO implements Serializable {

    private static final long serialVersionUID = -21069339610230226L;

    /**
     * 工单id
     */
    @ApiModelProperty(notes = "工单id")
    private Long id;

    /**
     * 检查日期
     */
    @NotNull
    @ApiModelProperty(notes = "检查日期")
    // @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date inspectDate;


    /**
     * 航班号
     */
    @NotNull
    @ApiModelProperty(notes = "航班号")
    private String flightNumber;


    /**
     * 机号
     */
    @NotNull
    @ApiModelProperty(notes = "机号")
    private String machineNumber;


    /**
     * 停机位
     */
    @NotNull
    @ApiModelProperty(notes = "停机位")
    private String stand;


    /**
     * 检查单类型 1接机工作 2送机工作  3维修检查  4长短停及航后离机检查项目
     */
    @NotNull
    @ApiModelProperty(notes = "检查单类型 1接机工作 2送机工作  3维修检查  4长短停及航后离机检查项目")
    private Integer type;


}
