package com.bicntech.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author jmy
 */
@Data
@ApiModel("外场审核结果")
public class OutfieldAuditWorkerDTO implements Serializable {

    /**
     * 工单id
     */
    @ApiModelProperty(notes = "工单id")
    @NotNull
    private Long id;

    /**
     * 观察员
     */
    @ApiModelProperty(notes = "观察员")
    @NotNull
    private String name;

    /**
     * 检查项
     */
    @ApiModelProperty(notes = "检查项")
    @NotNull
    private List<OutfieldInspectDTO> inspectDTOS;
}
