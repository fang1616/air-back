package com.bicntech.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author jmy
 */
@Data
@ApiModel("查询条件入参")
public class OutfieldInspectQueryDTO implements Serializable {

    private static final long serialVersionUID = -8715634041328825501L;

    /**
     * 检查单类型 1接机工作 2送机工作  3维修检查  4长短停及航后离机检查项目
     */
    @ApiModelProperty(required = false, notes = "检查单类型 1接机工作 2送机工作  3维修检查  4长短停及航后离机检查项目")
    private Integer type;


    /**
     * 检查单状态 1待检查  2待复核  3 已复核 4 废弃
     */
    @ApiModelProperty(required = false, notes = "检查单状态 1待检查  2待复核  3 历史 4 废弃")
    private Integer state;


    /**
     * 机号
     */
    @ApiModelProperty(required = false, notes = "机号")
    private String machineNumber;


    /**
     * 停机位
     */
    @ApiModelProperty(required = false, notes = "停机位")
    private String stand;


    /**
     * 开始时间
     */
    @ApiModelProperty(required = false, notes = "开始时间")
    private Date startTime;

    /**
     * 结束时间
     */
    @ApiModelProperty(required = false, notes = "结束时间")
    private Date endTime;

    /**
     * 航班号
     */
    @ApiModelProperty(required = false, notes = "航班号")
    private String flightNumber;

}
