package com.bicntech.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 22958
 */
@Data
@ApiModel("补加油量修改入参")
public class InfieldRefuelingVolumeDTO implements Serializable {


    @ApiModelProperty(required = true, notes = "id")
    @NotNull(message = "id is not null")
    private Long id;

    /**
     * 补加油量
     */
    @ApiModelProperty(required = true, notes = "补加油量")
    @NotNull(message = "refuelingVolume is not null")
    private BigDecimal refuelingVolume;
}
