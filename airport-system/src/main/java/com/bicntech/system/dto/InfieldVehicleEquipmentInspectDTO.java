package com.bicntech.system.dto;


import com.bicntech.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * @author bicntech
 * @description:机务维修记录--内场 传输实体类
 * </p>
 * @date 2022-08-02
 */
@ApiModel("机务维修记录--内场")
@Data
public class InfieldVehicleEquipmentInspectDTO extends BaseEntity {


    private static final long serialVersionUID = 5776416183625069373L;

    @ApiModelProperty(required = false, notes = "id")
    private Long id;

    /**
     * 检查单编号
     */
    @ApiModelProperty(required = false, notes = "检查单编号")
    private String code;


    /**
     * 检查单id
     */
    @ApiModelProperty(required = false, notes = "检查单id")
    private Long type;


    /**
     * 检查日期
     */
    @ApiModelProperty(required = false, notes = "检查日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date inspectDate;

    /**
     * 适用车型
     */
    @ApiModelProperty(required = false, notes = "适用车型")
    private String applicableModels;


    /**
     * 车号
     */
    @ApiModelProperty(required = false, notes = "车号")
    private String wagonNumber;


    /**
     * 编写人
     */
    @ApiModelProperty(required = false, notes = "编写人")
    private String preparedBy;


    /**
     * 审核
     */
    @ApiModelProperty(required = false, notes = "审核")
    private String toExamine;


    /**
     * 批准
     */
    @ApiModelProperty(required = false, notes = "批准")
    private String approval;


    /**
     * 检查项
     */
    @ApiModelProperty(required = false, notes = "检查项")
    private List<InfieldInspectDataDTO> inspectTerm;


    /**
     * 备注
     */
    @ApiModelProperty(required = false, notes = "备注")
    private String remarks;


    /**
     * 车辆摩托小时
     */
    @ApiModelProperty(required = false, notes = "车辆摩托小时")
    private BigDecimal vehicleMotorcycleHours;


    /**
     * 补加油量
     */
    @ApiModelProperty(required = false, notes = "补加油量")
    private BigDecimal refuelingVolume;


    /**
     * 检查人签名
     */
    @ApiModelProperty(required = false, notes = "检查人签名")
    private String signatureOfInspector;

    /**
     * 异常项
     */
    @ApiModelProperty(required = false, notes = "异常项")
    private String isAbnormal;

    /**
     * 版本号
     */
    @ApiModelProperty(notes = "版本号")
    private Integer version;


}
