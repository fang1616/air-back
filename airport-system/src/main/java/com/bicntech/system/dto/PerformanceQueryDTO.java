package com.bicntech.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 绩效查询条件
 *
 * @author : JMY
 * @create 2023/8/9 11:28
 */
@Data
@ApiModel("绩效查询条件")
public class PerformanceQueryDTO implements Serializable {
    private static final long serialVersionUID = 1953184309811142954L;

    /**
     * 人员名称
     */
    @ApiModelProperty(value = "人员名称")
    private String userName;

    /**
     * 加分场景 1 内场 2 外场
     */
    @ApiModelProperty(value = "加分场景 1 内场 2 外场")
    private Integer sceneType;

    /**
     * 年月
     */
    @ApiModelProperty(value = "年月 格式：yyyy-MM")
    private String years;


    /**
     * 组织id
     */
    @ApiModelProperty(value = "组织id")
    private Long departmentId;
}
