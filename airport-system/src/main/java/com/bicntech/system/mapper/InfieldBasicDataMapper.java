package com.bicntech.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bicntech.system.entity.InfieldBasicData;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


/**
* <p>
* @description:内场检查项基础数据 Mapper 接口
* </p>
*
* @author bicntech
* @date 2022-08-02
*/
@Mapper
@Repository
public interface InfieldBasicDataMapper extends BaseMapper<InfieldBasicData> {

}
