package com.bicntech.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bicntech.system.entity.WechatUserDept;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


/**
 * <p>
 *
 * @author bicntech
 * @description:微信用户组织管理 Mapper 接口
 * </p>
 * @date 2023-08-16
 */
@Mapper
@Repository
public interface WechatUserDeptMapper extends BaseMapper<WechatUserDept> {

}
