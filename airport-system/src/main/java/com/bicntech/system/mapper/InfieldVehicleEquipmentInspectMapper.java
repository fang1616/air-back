package com.bicntech.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bicntech.system.entity.InfieldVehicleEquipmentInspect;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


/**
* <p>
* @description:机务维修记录--内场 Mapper 接口
* </p>
*
* @author bicntech
* @date 2022-08-02
*/
@Mapper
@Repository
public interface InfieldVehicleEquipmentInspectMapper extends BaseMapper<InfieldVehicleEquipmentInspect> {

}
