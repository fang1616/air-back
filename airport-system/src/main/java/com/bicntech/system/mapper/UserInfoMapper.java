package com.bicntech.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bicntech.system.entity.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


/**
* <p>
* @description:用户信息表 Mapper 接口
* </p>
*
* @author bicntech
* @date 2022-08-31
*/
@Mapper
@Repository
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
