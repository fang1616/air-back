package com.bicntech.system.mapper;

import com.bicntech.common.core.mapper.BaseMapperPlus;
import com.bicntech.system.entity.SysConfig;

/**
 * 参数配置 数据层
 *
 * @author Lion Li
 */
public interface SysConfigMapper extends BaseMapperPlus<SysConfigMapper, SysConfig, SysConfig> {

}
