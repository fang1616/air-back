package com.bicntech.system.mapper;

import com.bicntech.common.core.mapper.BaseMapperPlus;
import com.bicntech.system.entity.SysRoleMenu;

/**
 * 角色与菜单关联表 数据层
 *
 * @author Lion Li
 */
public interface SysRoleMenuMapper extends BaseMapperPlus<SysRoleMenuMapper, SysRoleMenu, SysRoleMenu> {

}
