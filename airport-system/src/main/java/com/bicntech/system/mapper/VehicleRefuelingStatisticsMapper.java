package com.bicntech.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bicntech.system.entity.VehicleRefuelingStatistics;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


/**
 * <p>
 *
 * @author bicntech
 * @description:车辆加油统计 Mapper 接口
 * </p>
 * @date 2023-08-09
 */
@Mapper
@Repository
public interface VehicleRefuelingStatisticsMapper extends BaseMapper<VehicleRefuelingStatistics> {

}
