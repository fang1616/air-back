package com.bicntech.system.mapper;

import com.bicntech.common.core.mapper.BaseMapperPlus;
import com.bicntech.system.entity.SysLogininfor;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author Lion Li
 */
public interface SysLogininforMapper extends BaseMapperPlus<SysLogininforMapper, SysLogininfor, SysLogininfor> {

}
