package com.bicntech.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bicntech.system.entity.PerformanceScoreConfig;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


/**
 * <p>
 *
 * @author bicntech
 * @description:绩效加分项配置 Mapper 接口
 * </p>
 * @date 2023-08-07
 */
@Mapper
@Repository
public interface PerformanceScoreConfigMapper extends BaseMapper<PerformanceScoreConfig> {

}
