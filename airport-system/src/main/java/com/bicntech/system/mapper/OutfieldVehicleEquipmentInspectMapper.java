package com.bicntech.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bicntech.system.entity.OutfieldVehicleEquipmentInspect;
import com.bicntech.system.vo.OutfieldStateStatisticsVO;
import com.bicntech.system.vo.OutfieldTypeStatisticsVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>
 *
 * @author bicntech
 * @description:机务维修记录--外场 Mapper 接口
 * </p>
 * @date 2022-08-02
 */
@Mapper
@Repository
public interface OutfieldVehicleEquipmentInspectMapper extends BaseMapper<OutfieldVehicleEquipmentInspect> {

    @Select("SELECT state , COUNT(*) AS count FROM outfield_vehicle_equipment_inspect WHERE del_flag = '0' GROUP BY state ORDER BY state")
    List<OutfieldStateStatisticsVO> stateStatistics();

    @Select("SELECT type , COUNT(*) AS count FROM outfield_vehicle_equipment_inspect WHERE del_flag = '0' GROUP BY type ORDER BY type")
    List<OutfieldTypeStatisticsVO> typeStatistics();
}
