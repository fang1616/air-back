package com.bicntech.system.mapper;

import com.bicntech.common.core.domain.entity.SysDictType;
import com.bicntech.common.core.mapper.BaseMapperPlus;

/**
 * 字典表 数据层
 *
 * @author Lion Li
 */
public interface SysDictTypeMapper extends BaseMapperPlus<SysDictTypeMapper, SysDictType, SysDictType> {

}
