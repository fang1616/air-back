package com.bicntech.system.mapper;

import com.bicntech.common.core.mapper.BaseMapperPlus;
import com.bicntech.system.entity.SysUserPost;

/**
 * 用户与岗位关联表 数据层
 *
 * @author Lion Li
 */
public interface SysUserPostMapper extends BaseMapperPlus<SysUserPostMapper, SysUserPost, SysUserPost> {

}
