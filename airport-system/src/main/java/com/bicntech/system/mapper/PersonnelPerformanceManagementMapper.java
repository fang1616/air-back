package com.bicntech.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bicntech.system.entity.PersonnelPerformanceManagement;
import com.bicntech.system.vo.PersonnelPerformanceManagementVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>
 *
 * @author bicntech
 * @description:人员绩效管理 Mapper 接口
 * </p>
 * @date 2023-08-07
 */
@Mapper
@Repository
public interface PersonnelPerformanceManagementMapper extends BaseMapper<PersonnelPerformanceManagement> {


    List<PersonnelPerformanceManagementVO> selectUserPerformanceByYears(String years);
}
