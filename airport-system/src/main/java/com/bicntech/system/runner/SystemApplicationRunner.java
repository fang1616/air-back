package com.bicntech.system.runner;

import com.bicntech.common.config.AirportConfig;
import com.bicntech.system.service.ISysConfigService;
import com.bicntech.system.service.ISysDictTypeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 初始化 system 模块对应业务数据
 *
 * @author Lion Li
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class SystemApplicationRunner implements ApplicationRunner {

    private final AirportConfig airportConfig;
    private final ISysConfigService configService;
    private final ISysDictTypeService dictTypeService;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (airportConfig.isCacheLazy()) {
            return;
        }
        configService.loadingConfigCache();
        log.info("加载参数缓存数据成功");
        dictTypeService.loadingDictCache();
        log.info("加载字典缓存数据成功");
    }

}
