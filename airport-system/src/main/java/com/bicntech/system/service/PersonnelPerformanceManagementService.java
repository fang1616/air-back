package com.bicntech.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bicntech.system.dto.InfieldBasicDataDTO;
import com.bicntech.system.dto.PerformanceQueryDTO;
import com.bicntech.system.entity.PersonnelPerformanceManagement;
import com.bicntech.system.vo.PersonnelPerformanceManagementVO;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * @author bicntech
 * @description:人员绩效管理 服务类
 * </p>
 * @date 2023-08-07
 */
public interface PersonnelPerformanceManagementService extends IService<PersonnelPerformanceManagement> {

    IPage<PersonnelPerformanceManagementVO> listByPage(IPage<PersonnelPerformanceManagement> packPage, PerformanceQueryDTO queryDTO);

    /**
     * 增加评分
     *
     * @param userId         用户企业id
     * @param sceneType      加分场景 1 内场 2 外场
     * @param inspectionItem 内场巡检单类型id或外场巡检单类型
     * @param inspectDate    巡检单时间
     */
    void addMark(String userId, Integer sceneType, Long inspectionItem, Date inspectDate);

    /**
     * 撤回评分
     *
     * @param userId         用户企业id
     * @param sceneType      加分场景 1 内场 2 外场
     * @param inspectionItem 内场巡检单类型id或外场巡检单类型
     * @param inspectDate    巡检单时间
     */
    void recallMark(String userId, Integer sceneType, Long inspectionItem, Date inspectDate);

    List<PersonnelPerformanceManagementVO> selectListByYears(String years, List<InfieldBasicDataDTO> basicDataDTOList);
}
