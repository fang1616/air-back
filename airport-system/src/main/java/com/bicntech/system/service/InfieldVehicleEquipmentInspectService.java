package com.bicntech.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bicntech.system.dto.InfieldBasicDataQueryDTO;
import com.bicntech.system.dto.InfieldInspectAddDTO;
import com.bicntech.system.dto.InfieldRefuelingVolumeDTO;
import com.bicntech.system.dto.InfieldVehicleEquipmentInspectDTO;
import com.bicntech.system.entity.InfieldVehicleEquipmentInspect;

import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 *
 * @author bicntech
 * @description:机务维修记录--内场 服务类
 * </p>
 * @date 2022-08-02
 */
public interface InfieldVehicleEquipmentInspectService extends IService<InfieldVehicleEquipmentInspect> {

    /**
     * 分页查询List<InfieldVehicleEquipmentInspectDTO>
     *
     * @param iPage    分页
     * @param queryDTO 搜索实体
     * @return 返回mybatis-plus的Page对象,其中records字段为符合条件的查询结果
     * @author bicntech
     * @since 2022-08-02
     */
    IPage<InfieldVehicleEquipmentInspectDTO> listInfieldVehicleEquipmentInspectsByPage(IPage<InfieldVehicleEquipmentInspect> iPage, InfieldBasicDataQueryDTO queryDTO);

    /**
     * 条件查询列表
     *
     * @param queryDTO 条件
     * @return 列表
     */
    List<InfieldVehicleEquipmentInspect> selectList(InfieldBasicDataQueryDTO queryDTO);

    /**
     * 列表查询List<InfieldVehicleEquipmentInspectDTO>
     *
     * @param infieldVehicleEquipmentInspectDTO 搜索实体
     * @return 返回List<InfieldVehicleEquipmentInspectDTO>
     * @author bicntech
     * @since 2022-08-02
     */
    List<InfieldVehicleEquipmentInspectDTO> listInfieldVehicleEquipmentInspects(InfieldVehicleEquipmentInspectDTO infieldVehicleEquipmentInspectDTO);

    /**
     * 根据id查询InfieldVehicleEquipmentInspectDTO
     *
     * @param id 需要查询的InfieldVehicleEquipmentInspect的id
     * @return 返回对应id的InfieldVehicleEquipmentInspectDTO对象
     * @author bicntech
     * @since 2022-08-02
     */
    InfieldVehicleEquipmentInspectDTO getInfieldVehicleEquipmentInspectById(Long id);

    /**
     * 插入InfieldVehicleEquipmentInspect
     *
     * @param addDTO 需要插入对象
     * @return 返回插入成功之后InfieldVehicleEquipmentInspect对象的id
     * @author bicntech
     * @since 2022-08-02
     */
    Long addInfieldVehicleEquipmentInspect(InfieldInspectAddDTO addDTO);

    /**
     * 根据id删除InfieldVehicleEquipmentInspect
     *
     * @param id 需要删除的InfieldVehicleEquipmentInspect对象的id
     * @return 返回被删除的InfieldVehicleEquipmentInspect对象的id
     * @author bicntech
     * @since 2022-08-02
     */
    Long deleteInfieldVehicleEquipmentInspectById(Long id);

    /**
     * 根据id更新InfieldVehicleEquipmentInspect
     *
     * @param infieldVehicleEquipmentInspectDTO 需要更新的InfieldVehicleEquipmentInspect对象
     * @return 返回被更新的InfieldVehicleEquipmentInspect对象的id
     * @author bicntech
     * @since 2022-08-02
     */
    Long updateInfieldVehicleEquipmentInspect(InfieldVehicleEquipmentInspectDTO infieldVehicleEquipmentInspectDTO);

    /**
     * 修改巡检信息
     *
     * @param infieldRefuelingVolumeDTO 入参
     * @return id
     */
    Long updateInspect(InfieldRefuelingVolumeDTO infieldRefuelingVolumeDTO);

    /**
     * 删除过期数据
     *
     * @param validationDays 多少天之前的
     * @return 是否成功
     */
    Boolean deleteExpire(Integer validationDays);

    /**
     * 查询车辆今日是否已经巡检
     *
     * @param wagonNumber 车牌号
     * @param localDate   巡检日期
     * @return true 已巡检 false 未巡检
     */
    Boolean checkByWagonNumber(String wagonNumber, LocalDate localDate);

    /**
     * 今日已完成检查多少台特种车辆
     *
     * @return 数量
     */
    Long getInfieldSpecialVehicleCount();
}
