package com.bicntech.system.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bicntech.system.dto.UserDeptUpdateDTO;
import com.bicntech.system.dto.WechatUserDeptRequestDTO;
import com.bicntech.system.entity.WechatUserDept;

import java.util.List;

/**
 * <p>
 *
 * @author bicntech
 * @description:微信用户组织管理 服务类
 * </p>
 * @date 2023-08-16
 */
public interface WechatUserDeptService extends IService<WechatUserDept> {


    List<WechatUserDept> selectDeptList();

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    List<Tree<Long>> buildDeptTreeSelect(List<WechatUserDept> depts);

    /**
     * 校验部门名称是否唯一
     *
     * @param dept 部门信息
     * @return 结果
     */
    String checkDeptNameUnique(WechatUserDeptRequestDTO dept);

    Long insertDept(WechatUserDeptRequestDTO dept);

    Long updateDept(WechatUserDeptRequestDTO dept);

    boolean hasChildByDeptId(Long deptId);

    Long deleteDeptById(Long deptId);

    boolean checkDeptExistUser(Long deptId);

    void editUser(UserDeptUpdateDTO userDeptUpdateDTO);

    IPage<WechatUserDept> pageSelect(String deptName, Long parentId, IPage<WechatUserDept> packPage);

    WechatUserDept selectDeptById(Long deptId);
}
