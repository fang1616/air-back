package com.bicntech.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bicntech.common.core.domain.BaseEntity;
import com.bicntech.common.utils.ObjectUtil;
import com.bicntech.system.entity.UserInfo;
import com.bicntech.system.mapper.UserInfoMapper;
import com.bicntech.system.service.UserInfoService;
import com.bicntech.system.vo.UserInfoVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 *
 * @author bicntech
 * @description:用户信息表 服务实现类
 * </p>
 * @date 2022-08-31
 */
@Slf4j
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {


    @Override
    public UserInfo getByUserId(String userId) {
        return lambdaQuery().eq(UserInfo::getUserId, userId).one();
    }

    @Override
    public List<UserInfo> getByIds(List<String> ids) {
        return super.lambdaQuery().in(UserInfo::getUserId, ids).list();
    }

    @Override
    public List<String> selectByUserName(String userName, Long deptId) {
        return lambdaQuery()
                .like(StrUtil.isNotBlank(userName), UserInfo::getUserName, userName)
                .eq(ObjectUtil.notEmpty(deptId), UserInfo::getDepartmentId, deptId)
                .list()
                .stream()
                .map(UserInfo::getUserId)
                .collect(Collectors.toList());
    }

    @Override
    public IPage<UserInfoVO> getUserPage(IPage<UserInfo> packPage, String userName, Long deptId) {
        return super.lambdaQuery()
                .like(StrUtil.isNotBlank(userName), UserInfo::getUserName, userName)
                .eq(ObjectUtil.notEmpty(deptId), UserInfo::getDepartmentId, deptId)
                .orderByDesc(BaseEntity::getCreateTime)
                .page(packPage)
                .convert(ObjectUtil.transform(UserInfoVO.class));
    }
}
