package com.bicntech.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bicntech.common.core.domain.BaseEntity;
import com.bicntech.common.utils.ObjectUtil;
import com.bicntech.system.entity.PerformanceScoreConfig;
import com.bicntech.system.mapper.PerformanceScoreConfigMapper;
import com.bicntech.system.service.PerformanceScoreConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * <p>
 *
 * @author bicntech
 * @description:绩效加分项配置 服务实现类
 * </p>
 * @date 2023-08-07
 */
@Slf4j
@Service
public class PerformanceScoreConfigServiceImpl extends ServiceImpl<PerformanceScoreConfigMapper, PerformanceScoreConfig> implements PerformanceScoreConfigService {

    @Override
    public BigDecimal getScore(Integer sceneType, Long inspectionItem) {
        return Optional.ofNullable(lambdaQuery().eq(PerformanceScoreConfig::getSceneType, sceneType)
                .eq(PerformanceScoreConfig::getInspectionItem, inspectionItem)
                .orderByDesc(BaseEntity::getCreateTime)
                .last("LIMIT 1")
                .one())
                .filter(ObjectUtil::notEmpty)
                .map(PerformanceScoreConfig::getMark)
                .orElse(BigDecimal.valueOf(0.0));
    }
}
