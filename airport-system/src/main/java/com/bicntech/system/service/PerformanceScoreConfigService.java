package com.bicntech.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bicntech.system.entity.PerformanceScoreConfig;

;import java.math.BigDecimal;

/**
 * <p>
 *
 * @author bicntech
 * @description:绩效加分项配置 服务类
 * </p>
 * @date 2023-08-07
 */
public interface PerformanceScoreConfigService extends IService<PerformanceScoreConfig> {

    /**
     * 查询巡检项加分配置
     *
     * @param sceneType      加分场景 1 内场 2 外场
     * @param inspectionItem 巡检单id或类型
     * @return 分时配置
     */
    BigDecimal getScore(Integer sceneType, Long inspectionItem);


}
