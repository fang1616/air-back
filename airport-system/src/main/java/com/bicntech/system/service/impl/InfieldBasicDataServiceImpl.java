package com.bicntech.system.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bicntech.common.exception.ServiceException;
import com.bicntech.common.utils.ObjectUtil;
import com.bicntech.system.dto.InfieldBasicDataDTO;
import com.bicntech.system.entity.InfieldBasicData;
import com.bicntech.system.mapper.InfieldBasicDataMapper;
import com.bicntech.system.service.InfieldBasicDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *
 * @author bicntech
 * @description:内场检查项基础数据 服务实现类
 * </p>
 * @date 2022-08-02
 */
@Slf4j
@Service
public class InfieldBasicDataServiceImpl extends ServiceImpl<InfieldBasicDataMapper, InfieldBasicData> implements InfieldBasicDataService {

    @Override
    public IPage<InfieldBasicDataDTO> listInfieldBasicDatasByPage(int page, int pageSize, InfieldBasicDataDTO infieldBasicDataDTO) {
        QueryWrapper<InfieldBasicData> queryWrapper = new QueryWrapper<InfieldBasicData>();
        //TODO 这里需要自定义用于匹配的字段,并把wrapper传入下面的page方法
        IPage<InfieldBasicData> result = super.page(new Page<>(page, pageSize), queryWrapper);
        return result.convert(obj -> ObjectUtil.entityToModel(obj, InfieldBasicDataDTO.class));
    }

    @Override
    public List<InfieldBasicDataDTO> listInfieldBasicData() {
        List<InfieldBasicData> infieldBasicData = lambdaQuery().eq(InfieldBasicData::getEnable, true)
                .orderByAsc(InfieldBasicData::getType).list();
        return ObjectUtil.deepCopyList(infieldBasicData, InfieldBasicDataDTO.class);
    }

    @Override
    public InfieldBasicDataDTO getInfieldBasicDataById(Long id) {
        InfieldBasicData basicData = super.getById(id);
        if (ObjectUtil.isEmptyObject(basicData)) {
            return null;
        }
        return ObjectUtil.entityToModel(basicData, InfieldBasicDataDTO.class);

    }

    @Override
    public Long addInfieldBasicData(InfieldBasicDataDTO infieldBasicDataDTO) {
        InfieldBasicData infieldBasicData = ObjectUtil.entityToModel(infieldBasicDataDTO, InfieldBasicData.class);

        if (super.save(infieldBasicData)) {
            log.info("插入infieldBasicData成功,id为{}", infieldBasicData.getId());
            return infieldBasicData.getId();
        } else {
            throw new ServiceException("添加失败");
        }
    }

    @Override
    public Long deleteInfieldBasicDataById(Long id) {
        if (super.removeById(id)) {
            return id;
        } else {
            throw new ServiceException("删除失败[id=" + id + "]");
        }
    }

    @Override
    public Long updateInfieldBasicData(InfieldBasicDataDTO infieldBasicDataDTO) {
        InfieldBasicData infieldBasicData = ObjectUtil.entityToModel(infieldBasicDataDTO, InfieldBasicData.class);
        if (super.updateById(infieldBasicData)) {
            return infieldBasicData.getId();
        } else {
            throw new ServiceException("更新失败[id=" + infieldBasicData.getId() + "]");
        }
    }

    @Override
    public List<InfieldBasicData> getIsSpecialVehicle() {
        return lambdaQuery()
                .eq(InfieldBasicData::getIsSpecialVehicle, Boolean.TRUE)
                .list();
    }
}
