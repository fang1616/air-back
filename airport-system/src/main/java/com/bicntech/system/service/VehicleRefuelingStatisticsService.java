package com.bicntech.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bicntech.system.entity.VehicleRefuelingStatistics;
import com.bicntech.system.vo.VehicleRefuelingStatisticsVO;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * @author bicntech
 * @description:车辆加油统计 服务类
 * </p>
 * @date 2023-08-09
 */
public interface VehicleRefuelingStatisticsService extends IService<VehicleRefuelingStatistics> {


    void addRefueling(String wagonNumber, Date inspectDate, BigDecimal refuelingVolume);

    List<VehicleRefuelingStatisticsVO> annualStatistics(Integer year, String wagonNumber, Integer checklistType);
}
