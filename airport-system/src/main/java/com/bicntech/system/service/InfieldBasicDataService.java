package com.bicntech.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bicntech.system.dto.InfieldBasicDataDTO;
import com.bicntech.system.entity.InfieldBasicData;

import java.util.List;

/**
 * <p>
 *
 * @author bicntech
 * @description:内场检查项基础数据 服务类
 * </p>
 * @date 2022-08-02
 */
public interface InfieldBasicDataService extends IService<InfieldBasicData> {

    /**
     * 分页查询List<InfieldBasicDataDTO>
     *
     * @param page                当前页数
     * @param pageSize            页的大小
     * @param infieldBasicDataDTO 搜索实体
     * @return 返回mybatis-plus的Page对象,其中records字段为符合条件的查询结果
     * @author bicntech
     * @since 2022-08-02
     */
    IPage<InfieldBasicDataDTO> listInfieldBasicDatasByPage(int page, int pageSize, InfieldBasicDataDTO infieldBasicDataDTO);

    /**
     * 列表查询List<InfieldBasicDataDTO>
     *
     * @return 返回List<InfieldBasicDataDTO>
     * @author bicntech
     * @since 2022-08-02
     */
    List<InfieldBasicDataDTO> listInfieldBasicData();

    /**
     * 根据id查询InfieldBasicDataDTO
     *
     * @param id 需要查询的InfieldBasicData的id
     * @return 返回对应id的InfieldBasicDataDTO对象
     * @author bicntech
     * @since 2022-08-02
     */
    InfieldBasicDataDTO getInfieldBasicDataById(Long id);

    /**
     * 插入InfieldBasicData
     *
     * @param infieldBasicDataDTO 需要插入的InfieldBasicData对象
     * @return 返回插入成功之后InfieldBasicData对象的id
     * @author bicntech
     * @since 2022-08-02
     */
    Long addInfieldBasicData(InfieldBasicDataDTO infieldBasicDataDTO);

    /**
     * 根据id删除InfieldBasicData
     *
     * @param id 需要删除的InfieldBasicData对象的id
     * @return 返回被删除的InfieldBasicData对象的id
     * @author bicntech
     * @since 2022-08-02
     */
    Long deleteInfieldBasicDataById(Long id);

    /**
     * 根据id更新InfieldBasicData
     *
     * @param infieldBasicDataDTO 需要更新的InfieldBasicData对象
     * @return 返回被更新的InfieldBasicData对象的id
     * @author bicntech
     * @since 2022-08-02
     */
    Long updateInfieldBasicData(InfieldBasicDataDTO infieldBasicDataDTO);

    List<InfieldBasicData> getIsSpecialVehicle();
}
