package com.bicntech.system.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author : JMY
 * @create 2023/8/21 9:43
 */
public interface FileService {
    String uploadFile(MultipartFile file);

    Boolean deleteExpire(Integer validationDays);
}
