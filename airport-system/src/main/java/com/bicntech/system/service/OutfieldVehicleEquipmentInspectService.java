package com.bicntech.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bicntech.system.dto.*;
import com.bicntech.system.entity.OutfieldVehicleEquipmentInspect;
import com.bicntech.system.vo.OutfieldStateStatisticsVO;
import com.bicntech.system.vo.OutfieldTypeStatisticsVO;

import java.util.List;

/**
 * <p>
 *
 * @author bicntech
 * @description:机务维修记录--外场 服务类
 * </p>
 * @date 2022-08-02
 */
public interface OutfieldVehicleEquipmentInspectService extends IService<OutfieldVehicleEquipmentInspect> {

    /**
     * 分页查询List<OutfieldVehicleEquipmentInspectDTO>
     *
     * @param iPage 分页参数
     * @param query 搜索实体
     * @return 返回mybatis-plus的Page对象,其中records字段为符合条件的查询结果
     * @author bicntech
     * @since 2022-08-02
     */
    IPage<OutfieldVehicleEquipmentInspectDTO> listOutfieldVehicleEquipmentInspectsByPage(IPage<OutfieldVehicleEquipmentInspect> iPage, OutfieldInspectQueryDTO query);

    /**
     * 根据条件查询列表
     *
     * @param query 条件
     * @return 列表
     */
    List<OutfieldVehicleEquipmentInspect> selectList(OutfieldInspectQueryDTO query);

    /**
     * 列表查询List<OutfieldVehicleEquipmentInspectDTO>
     *
     * @param outfieldVehicleEquipmentInspectDTO 搜索实体
     * @return 返回List<OutfieldVehicleEquipmentInspectDTO>
     * @author bicntech
     * @since 2022-08-02
     */
    List<OutfieldVehicleEquipmentInspectDTO> listOutfieldVehicleEquipmentInspects(OutfieldVehicleEquipmentInspectDTO outfieldVehicleEquipmentInspectDTO);

    /**
     * 根据id查询OutfieldVehicleEquipmentInspectDTO
     *
     * @param id 需要查询的OutfieldVehicleEquipmentInspect的id
     * @return 返回对应id的OutfieldVehicleEquipmentInspectDTO对象
     * @author bicntech
     * @since 2022-08-02
     */
    OutfieldVehicleEquipmentInspectDTO getOutfieldVehicleEquipmentInspectById(Long id);

    /**
     * 插入OutfieldVehicleEquipmentInspect
     *
     * @param outfieldVehicleEquipmentInspectDTO 需要插入的OutfieldVehicleEquipmentInspect对象
     * @return 返回插入成功之后OutfieldVehicleEquipmentInspect对象的id
     * @author bicntech
     * @since 2022-08-02
     */
    Long addOutfieldVehicleEquipmentInspect(OutfieldInspectAddDTO outfieldVehicleEquipmentInspectDTO);

    /**
     * 根据id删除OutfieldVehicleEquipmentInspect
     *
     * @param id 需要删除的OutfieldVehicleEquipmentInspect对象的id
     * @return 返回被删除的OutfieldVehicleEquipmentInspect对象的id
     * @author bicntech
     * @since 2022-08-02
     */
    Long deleteOutfieldVehicleEquipmentInspectById(Long id);

    /**
     * 根据id更新OutfieldVehicleEquipmentInspect
     *
     * @param inspectWorkerDTO 需要更新的OutfieldVehicleEquipmentInspect对象
     * @return 返回被更新的OutfieldVehicleEquipmentInspect对象的id
     * @author bicntech
     * @since 2022-08-02
     */
    Long updateOutfieldVehicleEquipmentInspect(OutfieldInspectWorkerDTO inspectWorkerDTO);

    /**
     * 复核者提交
     *
     * @param inspectWorkerDTO 提交信息
     * @return 返回被更新的OutfieldVehicleEquipmentInspect对象的id
     */
    Long toExamine(OutfieldAuditWorkerDTO inspectWorkerDTO);

    /**
     * 表头修改
     *
     * @param inspectWorkerDTO 参数
     * @return id
     */
    Long updateHeadById(OutfieldInspectAddDTO inspectWorkerDTO);

    /**
     * 废弃工单
     *
     * @param id 工单id
     * @return 是否成功
     */
    Boolean disuseById(Long id);

    /**
     * 外场工单状态统计
     *
     * @return 统计
     */
    List<OutfieldStateStatisticsVO> stateStatistics();

    /**
     * 工单类型统计
     *
     * @return 统计
     */
    List<OutfieldTypeStatisticsVO> typeStatistics();

    /**
     * 删除过期数据
     *
     * @param validationDays 多少天之前的
     * @return 是否成功
     */
    Boolean deleteExpire(Integer validationDays);
}
