package com.bicntech.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bicntech.system.entity.UserInfo;
import com.bicntech.system.vo.UserInfoVO;

import java.util.List;


/**
 * <p>
 *
 * @author bicntech
 * @description:用户信息表 服务类
 * </p>
 * @date 2022-08-31
 */
public interface UserInfoService extends IService<UserInfo> {
    UserInfo getByUserId(String userId);

    List<UserInfo> getByIds(List<String> ids);

    List<String> selectByUserName(String userName, Long deptId);

    IPage<UserInfoVO> getUserPage(IPage<UserInfo> packPage, String userName, Long deptId);
}
