ALTER TABLE `airport`.`infield_basic_data`
    ADD COLUMN `is_special_vehicle` tinyint(1) NULL COMMENT '是否为特种车辆检查单' AFTER `content`;


UPDATE infield_basic_data SET is_special_vehicle = 1 WHERE type !=18 ;

DELETE FROM infield_vehicle_equipment_inspect WHERE del_flag =2;

UPDATE infield_vehicle_equipment_inspect SET vehicle_motorcycle_hours= REGEXP_REPLACE(vehicle_motorcycle_hours, '[^0-9]+', '');
UPDATE infield_vehicle_equipment_inspect SET vehicle_motorcycle_hours = '0' WHERE vehicle_motorcycle_hours = '';
UPDATE infield_vehicle_equipment_inspect SET refueling_volume= REGEXP_REPLACE(refueling_volume, '[^0-9]+', '');
UPDATE infield_vehicle_equipment_inspect SET refueling_volume = '0' WHERE refueling_volume = '';
ALTER TABLE `airport`.`infield_vehicle_equipment_inspect`
    MODIFY COLUMN `vehicle_motorcycle_hours` decimal(15, 1) NULL DEFAULT NULL COMMENT '车辆摩托小时' AFTER `remarks`,
    MODIFY COLUMN `refueling_volume` decimal(15, 1) NULL DEFAULT NULL COMMENT '补加油量' AFTER `vehicle_motorcycle_hours`;