ALTER TABLE `airport`.`outfield_vehicle_equipment_inspect`
    ADD COLUMN `version` int NULL DEFAULT 0 COMMENT '版本号' AFTER `del_flag`;
ALTER TABLE `airport`.`infield_vehicle_equipment_inspect`
    ADD COLUMN `version` int NULL DEFAULT 0 COMMENT '版本号' AFTER `del_flag`;
ALTER TABLE `airport`.`outfield_vehicle_equipment_inspect`
    ADD COLUMN `reviewer_id` varchar(50) NULL COMMENT '复核者id' AFTER `reviewer`,
    ADD COLUMN `worker_id`   varchar(50) NULL COMMENT '工作者id' AFTER `worker`;
ALTER TABLE `airport`.`user_info`
    ADD COLUMN `department_id`   int         NULL COMMENT '用户组织id' AFTER `del_flag`,
    ADD COLUMN `department_name` varchar(64) NULL COMMENT '用户组织名称' AFTER `department_id`