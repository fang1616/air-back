/*
 Navicat Premium Data Transfer

 Source Server         : 杭州mysql
 Source Server Type    : MySQL
 Source Server Version : 80025 (8.0.25)
 Source Host           : hz.bicntech.com:9006
 Source Schema         : airport

 Target Server Type    : MySQL
 Target Server Version : 80025 (8.0.25)
 File Encoding         : 65001

 Date: 10/08/2023 14:51:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for performance_score_config
-- ----------------------------
DROP TABLE IF EXISTS `performance_score_config`;
CREATE TABLE `performance_score_config`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '加分项名称',
  `scene_type` tinyint(1) NULL DEFAULT NULL COMMENT '加分场景 1 内场 2 外场',
  `inspection_item` bigint NULL DEFAULT NULL COMMENT '巡检项',
  `mark` decimal(10, 1) NULL DEFAULT NULL COMMENT '分数',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '绩效加分项配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of performance_score_config
-- ----------------------------
INSERT INTO `performance_score_config` VALUES (1, '外场-接机', 2, 1, 1.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (2, '外场-送机', 2, 2, 2.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (3, '外场-维修检查', 2, 3, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (4, '外场-长短停及航后离机检查项目', 2, 4, 5.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (5, '内场-Goldhofer飞机牵引车日常检查单', 1, 1, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (6, '内场-有杆牵引车日常检查单', 1, 2, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (7, '内场-TLD-TPX-100-E飞机牵引车日常检查单', 1, 3, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (8, '内场-有杆牵引车日常检查单', 1, 4, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (9, '内场-电源车日常检查单', 1, 5, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (10, '内场-电源车日常检查单', 1, 6, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (11, '内场-电源车日常检查单', 1, 7, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (12, '内场-除冰加液车日常检查单', 1, 8, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (13, '内场-气源车日常检查单', 1, 9, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (14, '内场-气源车日常检查单', 1, 10, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (15, '内场-TLD飞机牵引车日常检查单', 1, 11, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (16, '内场-空调车日常检查单', 1, 12, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (17, '内场-除冰车日常检查单', 1, 13, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (18, '内场-除冰车日常检查单', 1, 14, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (19, '内场-除冰车日常检查单', 1, 15, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (20, '内场-除冰车日常检查单', 1, 16, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (21, '内场-除冰车日常检查单', 1, 17, 3.0, NULL, NULL, NULL, NULL, '0');
INSERT INTO `performance_score_config` VALUES (22, '内场-皮卡工具车检查单', 1, 18, 2.0, NULL, NULL, NULL, NULL, '0');

SET FOREIGN_KEY_CHECKS = 1;
