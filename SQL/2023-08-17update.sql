ALTER TABLE `airport`.`user_info`
    MODIFY COLUMN `department_id` bigint NULL DEFAULT NULL COMMENT '用户组织id' AFTER `del_flag`;
UPDATE user_info
SET department_id = NULL,
    department_name = NULL;
INSERT INTO `airport`.`sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `query_param`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (125, '企业微信用户管理', 0, 9, 'wechatUser', 'system/wechat/user/index', NULL, 1, 0, 'C', '0', '0', NULL, '#', '', NULL, '', NULL, '');
INSERT INTO `airport`.`sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `query_param`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (126, '企业微信部门管理', 0, 8, 'wechatDept', 'system/wechat/dept/index', NULL, 1, 0, 'C', '0', '0', NULL, '#', '', NULL, '', NULL, '');
ALTER TABLE `airport`.`sys_user`
    ADD COLUMN `canary` tinyint NULL DEFAULT NULL COMMENT 'true 访问预览' AFTER `remark`;