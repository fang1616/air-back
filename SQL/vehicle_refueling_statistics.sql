/*
 Navicat Premium Data Transfer

 Source Server         : 杭州mysql
 Source Server Type    : MySQL
 Source Server Version : 80025 (8.0.25)
 Source Host           : hz.bicntech.com:9006
 Source Schema         : airport

 Target Server Type    : MySQL
 Target Server Version : 80025 (8.0.25)
 File Encoding         : 65001

 Date: 10/08/2023 14:51:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for vehicle_refueling_statistics
-- ----------------------------
DROP TABLE IF EXISTS `vehicle_refueling_statistics`;
CREATE TABLE `vehicle_refueling_statistics`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `year` int NULL DEFAULT NULL COMMENT '年',
  `month` int NULL DEFAULT NULL COMMENT '月',
  `wagon_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '车号',
  `refueling_volume` decimal(10, 1) NULL DEFAULT NULL COMMENT '补加油量',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `version` int NULL DEFAULT 0 COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '车辆加油统计' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
