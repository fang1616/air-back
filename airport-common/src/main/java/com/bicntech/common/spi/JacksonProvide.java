package com.bicntech.common.spi;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The interface Jackson provide.
 */

public interface JacksonProvide {

    /**
     * Provide object mapper.
     *
     * @param objectMapper the object mapper
     */
    void provide(ObjectMapper objectMapper);


}
