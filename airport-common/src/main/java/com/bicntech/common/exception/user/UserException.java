package com.bicntech.common.exception.user;

import com.bicntech.common.exception.base.BaseException;

/**
 * 用户信息异常类
 *
 * @author 123
 */
public class UserException extends BaseException {
    private static final long serialVersionUID = 1L;

    public UserException(String code, Object... args) {
        super("user", code, args, null);
    }
}
