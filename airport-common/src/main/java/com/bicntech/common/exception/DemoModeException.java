package com.bicntech.common.exception;

/**
 * 演示模式异常
 *
 * @author 123
 */
public class DemoModeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public DemoModeException() {
    }
}
