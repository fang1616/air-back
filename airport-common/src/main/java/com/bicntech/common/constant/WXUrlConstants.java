package com.bicntech.common.constant;

/**
 * @author jmy
 */
public interface WXUrlConstants {

    /**
     * 获取用户code 参数1 企业id corpId 参数2 重定向url Webpage
     */
    String GET_CODE = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={corpId}&redirect_uri={Webpage}&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect";

    /**
     * 获取access_token 参数1 企业id  参数2 应用secret secret
     */
    String ACCESS_TOKEN = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpId}&corpsecret={secret}";

    /**
     * 获取用户身份 参数1 access_token  参数2 用户code
     */
    String USER_INFO = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token={access_token}&code={code}";

    /**
     * 获取用户通讯录信息 参数1 access_token  参数2 userid
     */
    String MAIL_LIST = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token={access_token}&userid={userId}";

    /**
     * 根据id查询组织信息
     */
    String DEPARTMENT_BY_ID = "https://qyapi.weixin.qq.com/cgi-bin/department/get?access_token={access_token}&id={id}";


    /***
     * 服务号获取code
     * 第一步：用户同意授权，获取code
     */
    String GET_FW_CODE = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={appId}&redirect_uri={redirect_uri}&response_type=code&scope=snsapi_userinfo#wechat_redirect&forcePopup=true";

    /**
     * 服务号获取access_token
     * 第二步：通过 code 换取网页授权access_token
     */
    String ACCESS_FW_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={appid}&secret={secret}&code={code}&grant_type=authorization_code";


    /**
     * 服务号刷新access_token
     * 第三步：刷新access_token（如果需要）
     */
    String REFRESH_ACCESS_FW_TOKEN = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid={appid}&grant_type=refresh_token&refresh_token={refresh_token}";

    /**
     * 服务号获取用户信息
     * 第四步：拉取用户信息(需 scope 为 snsapi_userinfo)
     */
    String USER_FW_INFO = "https://api.weixin.qq.com/sns/userinfo?access_token={access_token}&openid={openid}&lang=zh_CN";

    /**
     * 检验授权凭证（access_token）是否有效
     */
    String EFFECTIVE_ACCESS_FW_TOKEN = "https://api.weixin.qq.com/sns/auth?access_token={access_token}&openid={openid}";
}
