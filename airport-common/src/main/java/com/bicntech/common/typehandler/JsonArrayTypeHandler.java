package com.bicntech.common.typehandler;

import com.baomidou.mybatisplus.extension.handlers.AbstractJsonTypeHandler;
import com.bicntech.common.utils.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;

import java.util.List;

/**
 * @author lan
 */
@MappedJdbcTypes({JdbcType.VARCHAR, JdbcType.LONGVARCHAR, JdbcType.BLOB})
public class JsonArrayTypeHandler<T> extends AbstractJsonTypeHandler<List<T>> {

	@Override
	protected List<T> parse(String json) {
		return JSON.toJavaObject(json, new TypeReference<List<T>>() {
		});
	}

	@Override
	protected String toJson(List<T> obj) {
		return JSON.toJSONString(obj);
	}

}
