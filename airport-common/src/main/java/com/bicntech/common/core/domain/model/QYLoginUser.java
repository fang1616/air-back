package com.bicntech.common.core.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author jmy
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class QYLoginUser extends LoginUser {

    private static final long serialVersionUID = 1L;

    /**
     * 企业内部应用用户id
     */
    private String qyUserId;

}