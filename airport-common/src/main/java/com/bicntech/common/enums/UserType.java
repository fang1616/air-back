package com.bicntech.common.enums;

import com.bicntech.common.utils.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 设备类型
 * 针对多套 用户体系
 *
 * @author Lion Li
 */
@Getter
@AllArgsConstructor
public enum UserType {

    /**
     * pc端
     */
    SYS_USER("sys_user"),

    /**
     * app端
     */
    APP_USER("app_user"),

    /**
     * 公众号
     */
    GZH_USER("gzh_user"),

    /**
     * 企业微信自建应用用户
     */
    QYWX_USER("qywx_user"),

    /**
     * 小程序
     */
    XXX_USER("xx_user");

    private final String userType;

    public static UserType getUserType(String str) {
        for (UserType value : values()) {
            if (StringUtils.contains(str, value.getUserType())) {
                return value;
            }
        }
        throw new RuntimeException("'UserType' not found By " + str);
    }
}
