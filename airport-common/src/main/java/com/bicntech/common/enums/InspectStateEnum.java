package com.bicntech.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author JMY
 */

@Getter
@AllArgsConstructor
public enum InspectStateEnum {
    /**
     * 待检查
     */
    TO_BE_CHECKED(1, "待检查"),
    /**
     * 待复核
     */
    TO_BE_REVIEWED(2, "待复核"),
    /**
     * 历史
     */
    HISTORY(3, "历史"),
    /**
     * 废弃
     */
    DISUSE(4, "废弃");

    /**
     * 下标
     */
    private final Integer description;

    /**
     * 类型
     */
    private final String value;
}
