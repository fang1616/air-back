package com.bicntech.admin.jobhandler;

import com.bicntech.system.service.FileService;
import com.bicntech.system.service.InfieldVehicleEquipmentInspectService;
import com.bicntech.system.service.OutfieldVehicleEquipmentInspectService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 基础数据定时任务
 *
 * @author jmy
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class BasicsXxlJob {

    private static final Logger logger = LoggerFactory.getLogger(BasicsXxlJob.class);

    private final static Integer VALIDATION_DAYS = 30;

    private final InfieldVehicleEquipmentInspectService inspectService;
    private final OutfieldVehicleEquipmentInspectService outfieldService;
    private final FileService fileService;

    /**
     * 删除过期巡检数据 30天以前
     */
//    @XxlJob("validityBookstoreInfoPeriod")
    @Transactional(rollbackFor = Exception.class)
    @Scheduled(cron = "0 0 0 * * ?")
    public void validityBookstoreInfoPeriod() {
        logger.info("指定器:{},开始执行", "validityBookstoreInfoPeriod");
        log.info("删除过期数据定时任务，开始执行");
        inspectService.deleteExpire(VALIDATION_DAYS);
        outfieldService.deleteExpire(VALIDATION_DAYS);
        fileService.deleteExpire(VALIDATION_DAYS);
        log.info("删除过期数据定时任务，结束执行");
        logger.info("指定器:{},执行结束", "validityBookstoreInfoPeriod");
    }
}
