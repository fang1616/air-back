package com.bicntech.admin.controller.airport;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bicntech.common.core.controller.SuperController;
import com.bicntech.common.core.domain.R;
import com.bicntech.common.utils.poi.ExcelUtil;
import com.bicntech.system.dto.InfieldBasicDataDTO;
import com.bicntech.system.dto.PerformanceQueryDTO;
import com.bicntech.system.service.InfieldBasicDataService;
import com.bicntech.system.service.PersonnelPerformanceManagementService;
import com.bicntech.system.vo.PersonnelPerformanceManagementVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


/**
 * <p>
 *
 * @author bicntech
 * @description: 人员绩效管理 前端控制器
 * </p>
 * @date 2023-08-07
 * @
 */
@Api(tags = "人员绩效管理")
@Slf4j
@RestController
@RequestMapping("/personnel-performance-management")
public class PersonnelPerformanceManagementController extends SuperController {

    @Resource
    private PersonnelPerformanceManagementService personnelPerformanceManagementService;

    @Resource
    private InfieldBasicDataService infieldBasicDataService;

    /**
     * 查询分页数据
     */
    @ApiOperation(value = "查询分页数据")
    @PostMapping(value = "/listByPage")
    public R<IPage<PersonnelPerformanceManagementVO>> listByPage(@RequestBody PerformanceQueryDTO queryDTO) {
        IPage<PersonnelPerformanceManagementVO> pageList = personnelPerformanceManagementService.listByPage(packPage(), queryDTO);
        return R.ok(pageList);
    }

    @ApiOperation("导出")
    @PostMapping("/export")
    public void export(HttpServletResponse response,
                       @ApiParam("年月 格式：yyyy-MM")
                       @RequestParam("years") String years) {
        List<InfieldBasicDataDTO> basicDataDTOList = infieldBasicDataService.listInfieldBasicData();
        List<PersonnelPerformanceManagementVO> managementVOList = personnelPerformanceManagementService.selectListByYears(years, basicDataDTOList);
        ExcelUtil.easyUtil(response, "人员绩效" + years, this.getHeads(basicDataDTOList), this.getBody(managementVOList));
    }


    private List<String> getHeads(List<InfieldBasicDataDTO> basicDataDTOList) {
        List<String> heads = new ArrayList<>();
        heads.add("姓名");
        heads.add("组织名称");
        heads.add("检查类型");
        heads.add("年-月");
        heads.add("分数");
        heads.add("接机工作");
        heads.add("送机工作");
        heads.add("维修检查");
        heads.add("长短停及航后离机检查项目");
        Optional.ofNullable(basicDataDTOList).filter(CollUtil::isNotEmpty)
                .ifPresent(a -> a.forEach(f -> heads.add(f.getApplicableModels())));
        return heads;
    }

    private List<Map<String, Object>> getBody(List<PersonnelPerformanceManagementVO> managementVOList) {
        List<Map<String, Object>> list = new ArrayList<>(managementVOList.size());
        Map<String, Object> objectMap;
        for (PersonnelPerformanceManagementVO managementVO : managementVOList) {
            objectMap = new LinkedHashMap<>(28);
            objectMap.put("姓名", managementVO.getUserName());
            objectMap.put("组织名称", managementVO.getDepartmentName());
            objectMap.put("检查类型", StrUtil.equals("1", managementVO.getSceneType()) ? "内场检查" :
                    StrUtil.equals("2", managementVO.getSceneType()) ? "外场检查" : null);
            objectMap.put("年-月", managementVO.getYears());
            objectMap.put("分数", managementVO.getMark());
            if (MapUtil.isNotEmpty(managementVO.getInspectionItem())) {
                for (String key : managementVO.getInspectionItem().keySet()) {
                    objectMap.put(key, managementVO.getInspectionItem().get(key));
                }
            }
            list.add(objectMap);
        }
        return list;

    }
}
