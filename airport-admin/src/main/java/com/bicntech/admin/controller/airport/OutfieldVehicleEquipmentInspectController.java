package com.bicntech.admin.controller.airport;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bicntech.common.annotation.RepeatSubmit;
import com.bicntech.common.core.controller.SuperController;
import com.bicntech.common.core.domain.R;
import com.bicntech.common.utils.ObjectUtil;
import com.bicntech.common.utils.StringUtils;
import com.bicntech.common.utils.poi.ExcelUtil;
import com.bicntech.system.dto.*;
import com.bicntech.system.entity.OutfieldVehicleEquipmentInspect;
import com.bicntech.system.service.OutfieldVehicleEquipmentInspectService;
import com.bicntech.system.vo.OutfieldStateStatisticsVO;
import com.bicntech.system.vo.OutfieldTypeStatisticsVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 *
 * @author bicntech
 * @description: 机务维修记录--外场 前端控制器
 * </p>
 * @date 2022-08-02
 * @
 */
@Api(tags = "机务维修记录--工单管理（外场）")
@Slf4j
@RestController
@RequestMapping("/outfield-vehicle-equipment-inspect")
public class OutfieldVehicleEquipmentInspectController extends SuperController {

    @Resource
    private OutfieldVehicleEquipmentInspectService outfieldVehicleEquipmentInspectService;

    /**
     * 查询分页数据
     */
    @ApiOperation(value = "查询分页数据")
    @PostMapping(value = "/listByPage")
    public R<IPage<OutfieldVehicleEquipmentInspectDTO>> listByPage(@RequestBody OutfieldInspectQueryDTO query) {
        IPage<OutfieldVehicleEquipmentInspectDTO> pageList = outfieldVehicleEquipmentInspectService.listOutfieldVehicleEquipmentInspectsByPage(packPage(), query);
        return R.ok(pageList);
    }

    /**
     * 查询数据列表
     */
    @ApiIgnore
    @ApiOperation(value = "查询数据列表")
    @PostMapping(value = "/listNoPage")
    public R<List<OutfieldVehicleEquipmentInspectDTO>> listNoPage(@RequestBody OutfieldVehicleEquipmentInspectDTO outfieldVehicleEquipmentInspectDTO) {
        List<OutfieldVehicleEquipmentInspectDTO> outfieldVehicleEquipmentInspectList = outfieldVehicleEquipmentInspectService.listOutfieldVehicleEquipmentInspects(outfieldVehicleEquipmentInspectDTO);
        return R.ok(outfieldVehicleEquipmentInspectList);
    }


    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "通过id查询")
    @GetMapping(value = "/queryById")
    public R<OutfieldVehicleEquipmentInspectDTO> queryById(@RequestParam(name = "id") Long id) {
        OutfieldVehicleEquipmentInspectDTO outfieldVehicleEquipmentInspectDTO = outfieldVehicleEquipmentInspectService.getOutfieldVehicleEquipmentInspectById(id);
        return R.ok(outfieldVehicleEquipmentInspectDTO);
    }


    /**
     * 添加
     *
     * @param outfieldVehicleEquipmentInspectDTO
     * @return
     */
    @ApiOperation(value = "添加")
    @PostMapping(value = "/add")
    public R<?> addOutfieldVehicleEquipmentInspect(@RequestBody @Validated OutfieldInspectAddDTO outfieldVehicleEquipmentInspectDTO) {
        return R.ok(outfieldVehicleEquipmentInspectService.addOutfieldVehicleEquipmentInspect(outfieldVehicleEquipmentInspectDTO));
    }


    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "通过id删除")
    @DeleteMapping(value = "/delete/{id}")
    @RepeatSubmit(message = "不允许重复提交，请稍候再试")
    public R<?> delete(@PathVariable Long id) {
        outfieldVehicleEquipmentInspectService.deleteOutfieldVehicleEquipmentInspectById(id);
        return R.ok("删除成功!");
    }


    /**
     * 工作者填写
     */
    @ApiOperation(value = "工作者填写")
    @PutMapping(value = "/edit")
    public R<?> edit(@RequestBody @Validated OutfieldInspectWorkerDTO inspectWorkerDTO) {
        outfieldVehicleEquipmentInspectService.updateOutfieldVehicleEquipmentInspect(inspectWorkerDTO);
        return R.ok("提交成功!");
    }

    /**
     * 审核者填写
     */
    @ApiOperation(value = "审核者填写")
    @PutMapping(value = "/toExamine")
    public R<?> toExamine(@RequestBody @Validated OutfieldAuditWorkerDTO inspectWorkerDTO) {
        outfieldVehicleEquipmentInspectService.toExamine(inspectWorkerDTO);
        return R.ok("提交成功!");
    }

    /**
     * 表头修改
     */
    @ApiOperation(value = "表头修改")
    @PutMapping(value = "/updateHeadById")
    public R<?> updateHeadById(@RequestBody @Validated OutfieldInspectAddDTO inspectWorkerDTO) {
        outfieldVehicleEquipmentInspectService.updateHeadById(inspectWorkerDTO);
        return R.ok("提交成功!");
    }

    /**
     * 工单废弃
     */
    @ApiOperation(value = "工单废弃")
    @PutMapping(value = "/updateStateById/{id}")
    public R<?> disuseById(@PathVariable("id") Long id) {
        outfieldVehicleEquipmentInspectService.disuseById(id);
        return R.ok("提交成功!");
    }


    /**
     * 外场工单状态统计
     */
    @ApiOperation(value = "外场工单状态统计")
    @GetMapping(value = "/stateStatistics")
    public R<List<OutfieldStateStatisticsVO>> stateStatistics() {
        return R.ok(outfieldVehicleEquipmentInspectService.stateStatistics());
    }

    /**
     * 外场工单类型统计
     */
    @ApiOperation(value = "工单类型统计")
    @GetMapping(value = "/typeStatistics")
    public R<List<OutfieldTypeStatisticsVO>> typeStatistics() {
        return R.ok(outfieldVehicleEquipmentInspectService.typeStatistics());
    }

    @ApiOperation("导出")
    @PostMapping("/export")
    public void export(@RequestBody OutfieldInspectQueryDTO query, HttpServletResponse response) {
        List<OutfieldVehicleEquipmentInspect> outfieldVehicleEquipmentInspectList = outfieldVehicleEquipmentInspectService.selectList(query);
        List<OutfieldVehicleExcelDTO> vehicleExcelDTOList = outfieldVehicleEquipmentInspectList
                .stream()
                .map(a -> ObjectUtil.entityToModel(a, OutfieldVehicleExcelDTO.class)
                        .setState(stateConversion(a.getState()))
                        .setTypeName(typeConversion(a.getType()))
                        .setInspectDate(DateUtil.format(a.getInspectDate(), "yyyy-MM-dd HH:mm:ss")))
                .collect(Collectors.toList());
        ExcelUtil.exportExcel(vehicleExcelDTOList, "外场巡检记录", OutfieldVehicleExcelDTO.class, response);
    }


    private static String typeConversion(Integer type) {
        switch (type) {
            case 1:
                return "接机工作";
            case 2:
                return "送机工作";
            case 3:
                return "维修检查";
            case 4:
                return "长短停及航后离机检查项目";
            default:
                return StringUtils.EMPTY;
        }

    }

    private static String stateConversion(Integer state) {
        switch (state) {
            case 1:
                return "待检查";
            case 2:
                return "待复核";
            case 3:
                return "已完成";
            case 4:
                return "废弃";
            default:
                return StringUtils.EMPTY;
        }

    }
}
