package com.bicntech.admin.controller.airport;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bicntech.common.constant.UserConstants;
import com.bicntech.common.core.controller.SuperController;
import com.bicntech.common.core.domain.R;
import com.bicntech.common.core.validate.AddGroup;
import com.bicntech.common.core.validate.EditGroup;
import com.bicntech.system.dto.UserDeptUpdateDTO;
import com.bicntech.system.dto.WechatUserDeptRequestDTO;
import com.bicntech.system.entity.WechatUserDept;
import com.bicntech.system.service.UserInfoService;
import com.bicntech.system.service.WechatUserDeptService;
import com.bicntech.system.vo.UserInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 *
 * @author bicntech
 * @description:微信用户组织管理 前端控制器
 * </p>
 * @date 2023-08-16
 * @
 */
@Api(tags = "微信用户组织管理")
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/wechat-user-dept")
public class WechatUserDeptController extends SuperController {
    private final WechatUserDeptService wechatUserDeptService;

    private final UserInfoService userInfoService;

    /**
     * 获取微信用户部门列表
     */
    @ApiOperation("获取部门分页")
    @GetMapping("/pageSelect")
    public R<IPage<WechatUserDept>> pageSelect(@ApiParam("部门名称") @RequestParam(name = "deptName", required = false)
                                                       String deptName,
                                               @ApiParam("查询部门下级部门 部门id") @RequestParam(name = "deptId", required = false)
                                                       Long deptId) {
        return R.ok(wechatUserDeptService.pageSelect(deptName, deptId, packPage()));
    }


    /**
     * 获取微信用户部门列表
     */
    @ApiOperation("获取部门树")
    @GetMapping("/treeSelect")
    public R<List<Tree<Long>>> treeSelect() {
        List<WechatUserDept> depts = wechatUserDeptService.selectDeptList();
        return R.ok(wechatUserDeptService.buildDeptTreeSelect(depts));
    }

    /**
     * 获取微信用户部门列表
     */
    @ApiOperation("获取微信用户部门列表")
    @GetMapping("/listSelect")
    public R<List<WechatUserDept>> listSelect() {
        return R.ok(wechatUserDeptService.selectDeptList());
    }

    /**
     * 新增部门
     */
    @ApiOperation("新增部门")
    @PostMapping("/add")
    public R<?> add(@Validated(value = AddGroup.class) @RequestBody WechatUserDeptRequestDTO dept) {
        if (UserConstants.NOT_UNIQUE.equals(wechatUserDeptService.checkDeptNameUnique(dept))) {
            return R.fail("新增部门'" + dept.getDeptName() + "'失败，部门名称已存在");
        }
        dept.setDeptId(null);
        wechatUserDeptService.insertDept(dept);
        return R.ok("操作成功！");
    }

    /**
     * 修改部门
     */
    @ApiOperation("修改部门")
    @PutMapping
    public R<?> edit(@Validated(value = EditGroup.class) @RequestBody WechatUserDeptRequestDTO dept) {
        Long deptId = dept.getDeptId();
        if (UserConstants.NOT_UNIQUE.equals(wechatUserDeptService.checkDeptNameUnique(dept))) {
            return R.fail("修改部门'" + dept.getDeptName() + "'失败，部门名称已存在");
        } else if (dept.getParentId().equals(deptId)) {
            return R.fail("修改部门'" + dept.getDeptName() + "'失败，上级部门不能是自己");
        }
        wechatUserDeptService.updateDept(dept);
        return R.ok("操作成功！");
    }

    /**
     * 删除部门
     */
    @ApiOperation("删除部门")
    @DeleteMapping("/{deptId}")
    public R<Void> remove(@ApiParam(value = "部门ID串", required = true) @PathVariable Long deptId) {
        if (wechatUserDeptService.hasChildByDeptId(deptId)) {
            return R.fail("存在下级部门,不允许删除");
        }
        if (wechatUserDeptService.checkDeptExistUser(deptId)) {
            return R.fail("部门存在用户,不允许删除");
        }
        wechatUserDeptService.deleteDeptById(deptId);
        return R.ok("操作成功！");
    }

    /**
     * 根据部门编号获取详细信息
     */
    @ApiOperation("根据部门编号获取详细信息")
    @GetMapping(value = "/{deptId}")
    public R<WechatUserDept> getInfo(@ApiParam("部门ID") @PathVariable Long deptId) {
        return R.ok(wechatUserDeptService.selectDeptById(deptId));
    }


    /**
     * 编辑用户部门
     */
    @ApiOperation("编辑用户部门")
    @PutMapping("editUser")
    public R<?> editUser(@Validated @RequestBody UserDeptUpdateDTO userDeptUpdateDTO) {
        wechatUserDeptService.editUser(userDeptUpdateDTO);
        return R.ok("操作成功！");
    }

    /**
     * 获取企业微信用户分页
     */
    @ApiOperation("获取企业微信用户分页")
    @GetMapping("getUserPage")
    public R<IPage<UserInfoVO>> getUserPage(
            @ApiParam("用户名") @RequestParam(name = "userName", required = false) String userName,
            @ApiParam("组织id") @RequestParam(name = "deptId", required = false) Long deptId) {
        return R.ok(userInfoService.getUserPage(packPage(), userName, deptId));
    }


}
