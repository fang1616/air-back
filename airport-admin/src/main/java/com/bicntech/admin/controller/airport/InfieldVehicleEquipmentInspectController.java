package com.bicntech.admin.controller.airport;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bicntech.common.annotation.RepeatSubmit;
import com.bicntech.common.core.controller.SuperController;
import com.bicntech.common.core.domain.R;
import com.bicntech.common.utils.ObjectUtil;
import com.bicntech.common.utils.poi.ExcelUtil;
import com.bicntech.system.dto.*;
import com.bicntech.system.entity.InfieldBasicData;
import com.bicntech.system.entity.InfieldVehicleEquipmentInspect;
import com.bicntech.system.service.InfieldBasicDataService;
import com.bicntech.system.service.InfieldVehicleEquipmentInspectService;
import com.bicntech.system.service.VehicleRefuelingStatisticsService;
import com.bicntech.system.vo.VehicleRefuelingStatisticsVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * <p>
 *
 * @author bicntech
 * @description:机务维修记录--内场 前端控制器
 * </p>
 * @date 2022-08-02
 * @
 */
@Api(tags = "机务维修记录--检查单管理（内场）")
@Slf4j
@Validated
@RestController
@RequestMapping("/infield-vehicle-equipment-inspect")
public class InfieldVehicleEquipmentInspectController extends SuperController {

    @Resource
    private InfieldVehicleEquipmentInspectService infieldVehicleEquipmentInspectService;

    @Resource
    private InfieldBasicDataService infieldBasicDataService;

    @Resource
    private VehicleRefuelingStatisticsService vehicleRefuelingStatisticsService;

    /**
     * 查询分页数据
     */
    @ApiOperation(value = "查询分页数据")
    @PostMapping(value = "/listByPage")
    public R<IPage<InfieldVehicleEquipmentInspectDTO>> listByPage(@RequestBody InfieldBasicDataQueryDTO queryDTO) {
        IPage<InfieldVehicleEquipmentInspectDTO> pageList =
                infieldVehicleEquipmentInspectService.listInfieldVehicleEquipmentInspectsByPage(packPage(), queryDTO);
        return R.ok(pageList);
    }

    /**
     * 查询数据列表
     */
    @ApiIgnore
    @ApiOperation(value = "查询数据列表")
    @PostMapping(value = "/listNoPage")
    public R<List<InfieldVehicleEquipmentInspectDTO>> listNoPage(@RequestBody InfieldVehicleEquipmentInspectDTO infieldVehicleEquipmentInspectDTO) {
        List<InfieldVehicleEquipmentInspectDTO> infieldVehicleEquipmentInspectList = infieldVehicleEquipmentInspectService.listInfieldVehicleEquipmentInspects(infieldVehicleEquipmentInspectDTO);
        return R.ok(infieldVehicleEquipmentInspectList);
    }


    /**
     * 通过id查询
     */
    @ApiOperation(value = "通过id查询")
    @GetMapping(value = "/queryById")
    public R<InfieldVehicleEquipmentInspectDTO> queryById(@RequestParam(name = "id", required = true) Long id) {
        InfieldVehicleEquipmentInspectDTO infieldVehicleEquipmentInspectDTO = infieldVehicleEquipmentInspectService.getInfieldVehicleEquipmentInspectById(id);
        return R.ok(infieldVehicleEquipmentInspectDTO);
    }


    /**
     * 添加
     */
    @ApiOperation(value = "添加")
    @PostMapping(value = "/add")
    @RepeatSubmit(message = "不允许重复提交，请稍候再试")
    public R<?> addInfieldVehicleEquipmentInspect(@RequestBody @Validated InfieldInspectAddDTO addDTO) {
        return R.ok(infieldVehicleEquipmentInspectService.addInfieldVehicleEquipmentInspect(addDTO));
    }


    /**
     * 通过id删除
     */
    @ApiOperation(value = "通过id删除")
    @DeleteMapping(value = "/delete/{id}")
    @RepeatSubmit(message = "不允许重复提交，请稍候再试")
    public R<?> delete(@PathVariable Long id) {
        infieldVehicleEquipmentInspectService.deleteInfieldVehicleEquipmentInspectById(id);
        return R.ok("删除成功!");
    }

    /**
     * 编辑
     */
    @ApiIgnore
    @ApiOperation(value = "编辑")
    @PutMapping(value = "/edit")
    public R<?> edit(@RequestBody @Validated InfieldVehicleEquipmentInspectDTO infieldVehicleEquipmentInspectDTO) {
        infieldVehicleEquipmentInspectService.updateInfieldVehicleEquipmentInspect(infieldVehicleEquipmentInspectDTO);
        return R.ok("编辑成功!");
    }

    /**
     * 编辑
     */
    @ApiOperation(value = "根据id修改")
    @PutMapping(value = "/update")
    public R<?> update(@RequestBody @Validated InfieldRefuelingVolumeDTO infieldRefuelingVolumeDTO) {
        infieldVehicleEquipmentInspectService.updateInspect(infieldRefuelingVolumeDTO);
        return R.ok("编辑成功!");
    }

    /**
     * 今日已完成检查多少台特种车辆
     */
    @ApiOperation(value = "今日已完成检查多少台特种车辆")
    @GetMapping(value = "/getInfieldSpecialVehicleCount")
    public R<?> getInfieldSpecialVehicleCount() {
        return R.ok(infieldVehicleEquipmentInspectService.getInfieldSpecialVehicleCount());
    }

    @ApiOperation("导出")
    @PostMapping("/export")
    public void export(@RequestBody InfieldBasicDataQueryDTO queryDTO, HttpServletResponse response) {
        List<InfieldVehicleEquipmentInspect> vehicleEquipmentInspectList = infieldVehicleEquipmentInspectService.selectList(queryDTO);
        List<InfieldVehicleExcelDTO> vehicleExcelDTOList = conversion(vehicleEquipmentInspectList);
        ExcelUtil.exportExcel(vehicleExcelDTOList, "内场巡检记录", InfieldVehicleExcelDTO.class, response);
    }

    /**
     * 获取年加油量统计
     */
    @ApiOperation(value = "获取年加油量统计")
    @GetMapping(value = "/annualStatistics")
    public R<List<VehicleRefuelingStatisticsVO>> annualStatistics(
            @NotNull(message = "年份不能为空")
            @ApiParam(value = "年份", required = true)
            @RequestParam("year") Integer year,
            @ApiParam(value = "车牌号")
            @RequestParam(value = "wagonNumber", required = false) String wagonNumber,
            @ApiParam(value = "检查单类型")
            @RequestParam(value = "checklistType", required = false) Integer checklistType) {
        return R.ok(vehicleRefuelingStatisticsService.annualStatistics(year, wagonNumber, checklistType));
    }

    private List<InfieldVehicleExcelDTO> conversion(List<InfieldVehicleEquipmentInspect> vehicleEquipmentInspectList) {
        if (CollUtil.isNotEmpty(vehicleEquipmentInspectList)) {
            Map<Long, InfieldBasicData> infieldBasicDataMap = infieldBasicDataService
                    .list()
                    .stream()
                    .collect(Collectors.toMap(InfieldBasicData::getId, Function.identity()
                            , (oldValue, newValue) -> newValue));

            return vehicleEquipmentInspectList
                    .stream()
                    .map(a -> ObjectUtil.entityToModel(a, InfieldVehicleExcelDTO.class)
                            .setTypeName(infieldBasicDataMap.get(a.getType()).getApplicableModels())
                            .setInspectDate(DateUtil.format(a.getInspectDate(), "yyyy-MM-dd")))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    /**
     * 查询车辆今日是否已经巡检
     */
    @ApiOperation(value = "查询车辆今日是否已经巡检")
    @GetMapping(value = "/checkByWagonNumber")
    public R<Boolean> checkByWagonNumber(@ApiParam(value = "车牌号", required = true)
                                         @RequestParam("wagonNumber") String wagonNumber,
                                         @ApiParam(value = "巡检日期", required = true)
                                         @RequestParam("localDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate localDate) {
        return R.ok(infieldVehicleEquipmentInspectService.checkByWagonNumber(wagonNumber, localDate));
    }


}
