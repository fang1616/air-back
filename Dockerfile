# Layered Jars with Spring Boot
# official https://spring.io/blog/2020/01/27/creating-docker-images-with-spring-boot-2-3-0-m1
# stackoverflow https://stackoverflow.com/questions/64195214/spring-boot-run-jarlauncher-when-extracting-jar

FROM openjdk:11-jre as builder
WORKDIR /application

# Add the Spring Boot application JAR
ARG TARGET_DIR
COPY $TARGET_DIR/target/*.jar ./application.jar
# extract the application
RUN java -Djarmode=layertools -jar ./application.jar extract

FROM openjdk:11-jre
WORKDIR /
COPY --from=builder /application/dependencies/ ./
COPY --from=builder /application/snapshot-dependencies/ ./
COPY --from=builder /application/spring-boot-loader/ ./
COPY --from=builder /application/application/ ./

ENV TZ=Asia/Shanghai

## Copy the agent files and other configurations
COPY ./agent /

#ENTRYPOINT ["java"]
#CMD ["-javaagent:/agent.jar","-Djava.security.egd=file:/dev/./urandom", "-Deaseagent.name=book", "-Deaseagent.log.conf=/easeagent-log4j2.xml", "-Deaseagent.config.path=/agent.yaml", "-Deaseagent.log.conf=/easeagent-log4j2.xml", "-Xms1024m", "-Xmx1024m", "-XX:+HeapDumpOnOutOfMemoryError", "-Dspring.profiles.active=${SPRING_PROFILE}", "-Xlog:gc:/gc.log", "org.springframework.boot.loader.JarLauncher"]

###############G1GC#################
#-XX:MaxGCPauseMillis=200: Sets a target for the maximum GC pause time. Lower values can improve application responsiveness at the cost of throughput.
#-XX:G1NewSizePercent=20: Sets the percentage of the heap to use as the minimum for the young generation size.
#-XX:G1ReservePercent=20: Sets the percentage of reserve memory to keep free to reduce the risk of to-space overflows.
#-XX:G1HeapRegionSize=32M: Sets the size of the G1 regions.

ENTRYPOINT ["java","-javaagent:/agent.jar", "-Djava.security.egd=file:/dev/./urandom", "-Deaseagent.log.conf=/easeagent-log4j2.xml", "-Deaseagent.config.path=/agent.yaml", "-Deaseagent.log.conf=/easeagent-log4j2.xml", "-XX:+UseG1GC", "-XX:+UnlockExperimentalVMOptions", "-XX:MaxGCPauseMillis=200", "-XX:G1NewSizePercent=20", "-XX:G1ReservePercent=20", "-XX:G1HeapRegionSize=32M", "-XX:+HeapDumpOnOutOfMemoryError", "-Dspring.profiles.active=${SPRING_PROFILE}", "-Xlog:gc:/gc.log"]

CMD [ "-Xms512m", "-Xmx2048m","-Deaseagent.name=airport", "org.springframework.boot.loader.JarLauncher"]

