package com.bicntech.app.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bicntech.common.core.controller.AppController;
import com.bicntech.common.core.controller.BaseController;
import com.bicntech.common.core.domain.R;
import com.bicntech.system.dto.InfieldBasicDataDTO;
import com.bicntech.system.service.InfieldBasicDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;


/**
 * <p>
 *
 * @author bicntech
 * @description: 内场检查项基础数据 前端控制器
 * </p>
 * @date 2022-08-02
 * @
 */
@Api(tags = "内场检查项基础数据")
@Slf4j
@RestController
@RequestMapping(AppController.BASE_URI + "/infield-basic-data")
public class InfieldBasicDataAPPController extends BaseController {

    @Resource
    private InfieldBasicDataService infieldBasicDataService;


    /**
     * 查询分页数据
     */
    @ApiIgnore
    @ApiOperation(value = "查询分页数据")
    @PostMapping(value = "/listByPage")
    public R<IPage<InfieldBasicDataDTO>> listByPage(@RequestParam(name = "page", defaultValue = "1") int pageNo,
                                                    @RequestParam(name = "limit", defaultValue = "10") int pageSize,
                                                    @RequestBody InfieldBasicDataDTO infieldBasicDataDTO) {
        IPage<InfieldBasicDataDTO> pageList = infieldBasicDataService.listInfieldBasicDatasByPage(pageNo, pageSize, infieldBasicDataDTO);
        return R.ok(pageList);
    }

    /**
     * 查询数据列表
     */
    @ApiOperation(value = "查询检查单类型/检查项列表")
    @GetMapping(value = "/listNoPage")
    public R<List<InfieldBasicDataDTO>> listNoPage() {
        List<InfieldBasicDataDTO> infieldBasicDataList = infieldBasicDataService.listInfieldBasicData();
        return R.ok(infieldBasicDataList);
    }


    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "通过id查询")
    @GetMapping(value = "/queryById")
    public R<InfieldBasicDataDTO> queryById(@RequestParam(name = "id", required = true) Long id) {
        InfieldBasicDataDTO infieldBasicDataDTO = infieldBasicDataService.getInfieldBasicDataById(id);
        return R.ok(infieldBasicDataDTO);
    }


    /**
     * 添加
     *
     * @param infieldBasicDataDTO
     * @return
     */
    @ApiIgnore
    @ApiOperation(value = "添加")
    @PostMapping(value = "/add")
    public R<?> addInfieldBasicData(@RequestBody @Validated InfieldBasicDataDTO infieldBasicDataDTO) {
        infieldBasicDataService.addInfieldBasicData(infieldBasicDataDTO);
        return R.ok("添加成功！");
    }


    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @ApiIgnore
    @ApiOperation(value = "通过id删除")
    @DeleteMapping(value = "/delete/{id}")
    public R<?> delete(@PathVariable Long id) {
        infieldBasicDataService.deleteInfieldBasicDataById(id);
        return R.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param list
     * @return
     */
    @ApiIgnore
    @ApiOperation(value = "批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public R<?> deleteBatch(@RequestBody List<Long> list) {
        infieldBasicDataService.removeByIds(list);
        return R.ok("批量删除成功！");
    }


    /**
     * 编辑
     *
     * @param infieldBasicDataDTO
     * @return
     */
    @ApiIgnore
    @ApiOperation(value = "编辑")
    @PutMapping(value = "/edit")
    public R<?> edit(@RequestBody @Validated InfieldBasicDataDTO infieldBasicDataDTO) {
        infieldBasicDataService.updateInfieldBasicData(infieldBasicDataDTO);
        return R.ok("编辑成功!");
    }


}
