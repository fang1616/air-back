package com.bicntech.app.controller;

import com.bicntech.common.core.controller.AppController;
import com.bicntech.common.core.controller.BaseController;
import com.bicntech.common.core.domain.R;
import com.bicntech.system.service.FileService;
import com.google.common.collect.ImmutableMap;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

/**
 * @author : JMY
 * @create 2023/8/21 9:46
 */
@Api(tags = "文件上传")
@Slf4j
@RestController
@RequestMapping(AppController.BASE_URI + "/file")
public class FileController extends BaseController {

    @Resource
    private FileService fileService;

    @ApiOperation(value = "上传图片")
    @PostMapping("/uploadFile")
    public R<?> uploadFile(@NotNull(message = "请勿上传空文件") @RequestPart MultipartFile file) {
        return R.ok(ImmutableMap.of("path", fileService.uploadFile(file)));
    }
}
