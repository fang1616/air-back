package com.bicntech.app.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bicntech.common.annotation.RepeatSubmit;
import com.bicntech.common.core.controller.AppController;
import com.bicntech.common.core.controller.BaseController;
import com.bicntech.common.core.domain.R;
import com.bicntech.system.dto.InfieldBasicDataQueryDTO;
import com.bicntech.system.dto.InfieldInspectAddDTO;
import com.bicntech.system.dto.InfieldRefuelingVolumeDTO;
import com.bicntech.system.dto.InfieldVehicleEquipmentInspectDTO;
import com.bicntech.system.service.InfieldVehicleEquipmentInspectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;


/**
 * <p>
 *
 * @author bicntech
 * @description:机务维修记录--内场 前端控制器
 * </p>
 * @date 2022-08-02
 * @
 */
@Api(tags = "机务维修记录--检查单管理（内场）")
@Slf4j
@RestController
@RequestMapping(AppController.BASE_URI + "/infield-vehicle-equipment-inspect")
public class InfieldVehicleEquipmentInspectAPPController extends BaseController {

    @Resource
    private InfieldVehicleEquipmentInspectService infieldVehicleEquipmentInspectService;

    /**
     * 查询分页数据
     */
    @ApiOperation(value = "查询分页数据")
    @PostMapping(value = "/listByPage")
    public R<IPage<InfieldVehicleEquipmentInspectDTO>> listByPage(@RequestBody InfieldBasicDataQueryDTO queryDTO) {
        IPage<InfieldVehicleEquipmentInspectDTO> pageList =
                infieldVehicleEquipmentInspectService.listInfieldVehicleEquipmentInspectsByPage(packPage(), queryDTO);
        return R.ok(pageList);
    }

    /**
     * 查询数据列表
     */
    @ApiIgnore
    @ApiOperation(value = "查询数据列表")
    @PostMapping(value = "/listNoPage")
    public R<List<InfieldVehicleEquipmentInspectDTO>> listNoPage(@RequestBody InfieldVehicleEquipmentInspectDTO infieldVehicleEquipmentInspectDTO) {
        List<InfieldVehicleEquipmentInspectDTO> infieldVehicleEquipmentInspectList = infieldVehicleEquipmentInspectService.listInfieldVehicleEquipmentInspects(infieldVehicleEquipmentInspectDTO);
        return R.ok(infieldVehicleEquipmentInspectList);
    }


    /**
     * 通过id查询
     */
    @ApiOperation(value = "通过id查询")
    @GetMapping(value = "/queryById")
    public R<InfieldVehicleEquipmentInspectDTO> queryById(@RequestParam(name = "id", required = true) Long id) {
        InfieldVehicleEquipmentInspectDTO infieldVehicleEquipmentInspectDTO = infieldVehicleEquipmentInspectService.getInfieldVehicleEquipmentInspectById(id);
        return R.ok(infieldVehicleEquipmentInspectDTO);
    }


    /**
     * 添加
     */
    @ApiOperation(value = "添加")
    @PostMapping(value = "/add")
    @RepeatSubmit(message = "不允许重复提交，请稍候再试")
    public R<?> addInfieldVehicleEquipmentInspect(@RequestBody @Validated InfieldInspectAddDTO addDTO) {
        return R.ok(infieldVehicleEquipmentInspectService.addInfieldVehicleEquipmentInspect(addDTO));
    }

    /**
     * 查询车辆今日是否已经巡检
     */
    @ApiOperation(value = "查询车辆今日是否已经巡检")
    @GetMapping(value = "/checkByWagonNumber")
    public R<Boolean> checkByWagonNumber(@ApiParam(value = "车牌号", required = true)
                                         @RequestParam("wagonNumber") String wagonNumber,
                                         @ApiParam(value = "巡检日期", required = true)
                                         @RequestParam("localDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate localDate) {
        return R.ok(infieldVehicleEquipmentInspectService.checkByWagonNumber(wagonNumber, localDate));
    }


    /**
     * 通过id删除
     */
    @ApiIgnore
    @ApiOperation(value = "通过id删除")
    @DeleteMapping(value = "/delete/{id}")
    public R<?> delete(@PathVariable Long id) {
        infieldVehicleEquipmentInspectService.deleteInfieldVehicleEquipmentInspectById(id);
        return R.ok("删除成功!");
    }

    /**
     * 编辑
     */
    @ApiIgnore
    @ApiOperation(value = "编辑")
    @PutMapping(value = "/edit")
    public R<?> edit(@RequestBody @Validated InfieldVehicleEquipmentInspectDTO infieldVehicleEquipmentInspectDTO) {
        infieldVehicleEquipmentInspectService.updateInfieldVehicleEquipmentInspect(infieldVehicleEquipmentInspectDTO);
        return R.ok("编辑成功!");
    }

    /**
     * 编辑
     */
    @ApiOperation(value = "根据id修改")
    @PutMapping(value = "/update")
    public R<?> update(@RequestBody @Validated InfieldRefuelingVolumeDTO infieldRefuelingVolumeDTO) {
        infieldVehicleEquipmentInspectService.updateInspect(infieldRefuelingVolumeDTO);
        return R.ok("编辑成功!");
    }
}
