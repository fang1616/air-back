package com.bicntech.app.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bicntech.common.annotation.RepeatSubmit;
import com.bicntech.common.core.controller.AppController;
import com.bicntech.common.core.controller.BaseController;
import com.bicntech.common.core.domain.R;
import com.bicntech.system.dto.*;
import com.bicntech.system.service.OutfieldVehicleEquipmentInspectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;


/**
 * <p>
 *
 * @author bicntech
 * @description: 机务维修记录--外场 前端控制器
 * </p>
 * @date 2022-08-02
 * @
 */
@Api(tags = "机务维修记录--工单管理（外场）")
@Slf4j
@RestController
@RequestMapping(AppController.BASE_URI + "/outfield-vehicle-equipment-inspect")
public class OutfieldVehicleEquipmentInspectAPPController extends BaseController {

    @Resource
    private OutfieldVehicleEquipmentInspectService outfieldVehicleEquipmentInspectService;

    /**
     * 查询分页数据
     */
    @ApiOperation(value = "查询分页数据")
    @PostMapping(value = "/listByPage")
    public R<IPage<OutfieldVehicleEquipmentInspectDTO>> listByPage(@RequestBody OutfieldInspectQueryDTO query) {
        IPage<OutfieldVehicleEquipmentInspectDTO> pageList = outfieldVehicleEquipmentInspectService.listOutfieldVehicleEquipmentInspectsByPage(packPage(), query);
        return R.ok(pageList);
    }

    /**
     * 查询数据列表
     */
    @ApiIgnore
    @ApiOperation(value = "查询数据列表")
    @PostMapping(value = "/listNoPage")
    public R<List<OutfieldVehicleEquipmentInspectDTO>> listNoPage(@RequestBody OutfieldVehicleEquipmentInspectDTO outfieldVehicleEquipmentInspectDTO) {
        List<OutfieldVehicleEquipmentInspectDTO> outfieldVehicleEquipmentInspectList = outfieldVehicleEquipmentInspectService.listOutfieldVehicleEquipmentInspects(outfieldVehicleEquipmentInspectDTO);
        return R.ok(outfieldVehicleEquipmentInspectList);
    }


    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "通过id查询")
    @GetMapping(value = "/queryById")
    public R<OutfieldVehicleEquipmentInspectDTO> queryById(@RequestParam(name = "id") Long id) {
        OutfieldVehicleEquipmentInspectDTO outfieldVehicleEquipmentInspectDTO = outfieldVehicleEquipmentInspectService.getOutfieldVehicleEquipmentInspectById(id);
        return R.ok(outfieldVehicleEquipmentInspectDTO);
    }


    /**
     * 添加
     *
     * @param outfieldVehicleEquipmentInspectDTO
     * @return
     */
    @ApiOperation(value = "添加")
    @PostMapping(value = "/add")
    @RepeatSubmit(message = "不允许重复提交，请稍候再试")
    public R<?> addOutfieldVehicleEquipmentInspect(@RequestBody @Validated OutfieldInspectAddDTO outfieldVehicleEquipmentInspectDTO) {
        return R.ok(outfieldVehicleEquipmentInspectService.addOutfieldVehicleEquipmentInspect(outfieldVehicleEquipmentInspectDTO));
    }


    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @ApiIgnore
    @ApiOperation(value = "通过id删除")
    @DeleteMapping(value = "/delete/{id}")
    public R<?> delete(@PathVariable Long id) {
        outfieldVehicleEquipmentInspectService.deleteOutfieldVehicleEquipmentInspectById(id);
        return R.ok("删除成功!");
    }


    /**
     * 工作者填写
     */
    @ApiOperation(value = "工作者填写")
    @PutMapping(value = "/edit")
    @RepeatSubmit(message = "不允许重复提交，请稍候再试")
    public R<?> edit(@RequestBody @Validated OutfieldInspectWorkerDTO inspectWorkerDTO) {
        outfieldVehicleEquipmentInspectService.updateOutfieldVehicleEquipmentInspect(inspectWorkerDTO);
        return R.ok("提交成功!");
    }

    /**
     * 审核者填写
     */
    @ApiOperation(value = "审核者填写")
    @PutMapping(value = "/toExamine")
    @RepeatSubmit(message = "不允许重复提交，请稍候再试")
    public R<?> toExamine(@RequestBody @Validated OutfieldAuditWorkerDTO inspectWorkerDTO) {
        inspectWorkerDTO.setName(getLoginUser().getUsername());
        outfieldVehicleEquipmentInspectService.toExamine(inspectWorkerDTO);
        return R.ok("提交成功!");
    }

    /**
     * 表头修改
     */
    @ApiOperation(value = "表头修改")
    @PutMapping(value = "/updateHeadById")
    public R<?> updateHeadById(@RequestBody @Validated OutfieldInspectAddDTO inspectWorkerDTO) {
        outfieldVehicleEquipmentInspectService.updateHeadById(inspectWorkerDTO);
        return R.ok("提交成功!");
    }

    /**
     * 工单废弃
     */
    @ApiOperation(value = "工单废弃")
    @PutMapping(value = "/updateStateById/{id}")
    @RepeatSubmit(message = "不允许重复提交，请稍候再试")
    public R<?> disuseById(@PathVariable("id") Long id) {
        outfieldVehicleEquipmentInspectService.disuseById(id);
        return R.ok("提交成功!");
    }
}
