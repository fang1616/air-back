package com.bicntech.framework.config;


import com.bicntech.common.utils.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.context.annotation.Bean;


/**
 * The type Json serializer manage.
 *
 * @description: Long型数据转换成json格式时丢失精度问题
 * @author: bicntech
 * @create: 2021 -01-18 13:50
 */
@JsonComponent
public class JsonSerializerManage {

    /**
     * Jackson objectMapper
     * Maybe SPI ReConf
     *
     * @return the object mapper
     */
    @Bean
    public ObjectMapper jacksonObjectMapper() {
        return JSON.getInstance();
    }


}
