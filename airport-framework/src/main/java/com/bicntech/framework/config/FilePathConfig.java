package com.bicntech.framework.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 图片映射
 *
 * @author : JMY
 * @create 2023/8/21 10:41
 */
@Configuration
@Component
public class FilePathConfig implements WebMvcConfigurer {

    @Value("${image.path}")
    private String filePath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/pub/acquire/**") //虚拟url路径
                .addResourceLocations("file:" + filePath); //真实本地路径
    }
}


