package com.bicntech.framework.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 微信配置
 */

@Data
@Component
@ConfigurationProperties(prefix = "wx.mp")
public class WXProperties {

    /**
     * 应用id
     */
    private String appId;

    /**
     * 应用秘钥
     */
    private String secret;

    /**
     * token
     */
    private String token;

    /**
     * aesKey
     */
    private String aesKey;
}
