package com.bicntech.framework.config;

import cn.dev33.satoken.interceptor.SaAnnotationInterceptor;
import cn.dev33.satoken.interceptor.SaRouteInterceptor;
import cn.dev33.satoken.jwt.StpLogicJwtForSimple;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpLogic;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.URLUtil;
import com.bicntech.common.core.controller.AppController;
import com.bicntech.common.enums.UserType;
import com.bicntech.common.exception.user.UserException;
import com.bicntech.common.helper.LoginHelper;
import com.bicntech.common.utils.spring.SpringUtils;
import com.bicntech.framework.config.properties.ExcludeUrlProperties;
import com.bicntech.framework.config.properties.SecurityProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * sa-token 配置
 *
 * @author Lion Li
 */
@RequiredArgsConstructor
@Slf4j
@Configuration
public class SaTokenConfig implements WebMvcConfigurer {

    private final SecurityProperties securityProperties;

    /**
     * 注册sa-token的拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SaAnnotationInterceptor()).addPathPatterns("/**");
        // 注册路由拦截器，自定义验证规则
        registry.addInterceptor(new SaRouteInterceptor((request, response, handler) -> {
            ExcludeUrlProperties excludeUrlProperties = SpringUtils.getBean(ExcludeUrlProperties.class);
            // 登录验证 -- 排除多个路径
            SaRouter
                    // 获取所有的
                    .match("/**")
                    // 排除下不需要拦截的
                    .notMatch(securityProperties.getExcludes())
                    .notMatch(excludeUrlProperties.getExcludes())
                    // 对未排除的路径进行检查
                    .check(() -> {
                        // 检查是否登录 是否有token
                        StpUtil.checkLogin();
                        // 检查访问的url-token 是否合法
                        if (UserType.QYWX_USER.equals(LoginHelper.getUserType())) {
                            if (URLUtil.getPath(request.getUrl()).indexOf(AppController.BASE_URI) != 0) {
                                throw new UserException("token.type.error", LoginHelper.getUsername());
                            }
                        }
                        // 有效率影响 用于临时测试
                        // if (log.isDebugEnabled()) {
                        //     log.debug("剩余有效时间: {}", StpUtil.getTokenTimeout());
                        //     log.debug("临时有效时间: {}", StpUtil.getTokenActivityTimeout());
                        // }

                    });
        })).addPathPatterns("/**");
    }

    @Bean
    public StpLogic getStpLogicJwt() {
        // Sa-Token 整合 jwt (简单模式)
        return new StpLogicJwtForSimple();
    }


}
